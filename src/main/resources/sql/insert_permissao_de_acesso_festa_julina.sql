-- Verificar se as tabelas foram criadas
SELECT * FROM SEC_USER
SELECT * FROM SEC_PERMISSION
SELECT * FROM SEC_USER_PERMISSION
SELECT * FROM SEC_PAGE_ACCESS

-- Inserir Usuário e senha (com md5)
INSERT INTO SEC_USER (USER_NAME, PASSWORD, IS_ENABLED)
VALUES ('admin', '21232f297a57a5a743894a0e4a801fc3', '1')

-- Inserir usuário com ID
INSERT INTO SEC_PERMISSION (NAME) VALUES ('admin')

-- Inserir usuário com ID
INSERT INTO SEC_USER_PERMISSION (ID_USER, ID_PERMISSION) VALUES (1, 1)

-- Inserir página + role para ID cadastrado
-- Tela: Menu
INSERT INTO SEC_PAGE_ACCESS (PAGE_URL, ROLE_NAME, ID_PERMISSION)
VALUES ('menu.jsf', 'ROLE_Menu', 1)

INSERT INTO SEC_PAGE_ACCESS (PAGE_URL, ROLE_NAME, ID_PERMISSION)
VALUES ('menuMobile.jsf', 'ROLE_MenuMobile', 1)

-- Tela: Busca Cadastro
INSERT INTO SEC_PAGE_ACCESS (PAGE_URL, ROLE_NAME, ID_PERMISSION) 
VALUES ('buscaCadFestaJulina.jsf', 'ROLE_buscaCadFestaJulina', 1)

INSERT INTO SEC_PAGE_ACCESS (PAGE_URL, ROLE_NAME, ID_PERMISSION) 
VALUES ('buscaCadFestaJulinaMobile.jsf', 'ROLE_buscaCadFestaJulinaMobile', 1)

-- Tela: Cadastro
INSERT INTO SEC_PAGE_ACCESS (PAGE_URL, ROLE_NAME, ID_PERMISSION) 
VALUES ('cadFestaJulina.jsf', 'ROLE_cadFestaJulina', 1)

INSERT INTO SEC_PAGE_ACCESS (PAGE_URL, ROLE_NAME, ID_PERMISSION) 
VALUES ('cadFestaJulinaMobile.jsf', 'ROLE_cadFestaJulinaMobile', 1)

-- Tela: Tabela
INSERT INTO SEC_PAGE_ACCESS (PAGE_URL, ROLE_NAME, ID_PERMISSION) 
VALUES ('tabelaCadFestaJulina.jsf', 'ROLE_tabelaCadFestaJulina', 1)

-- Tela: Upload
INSERT INTO SEC_PAGE_ACCESS (PAGE_URL, ROLE_NAME, ID_PERMISSION) 
VALUES ('uploadPlanilhaCadFestaJulina.jsf', 'ROLE_uploadPlanilhaCadFestaJulina', 1)

-- Tela: Download
INSERT INTO SEC_PAGE_ACCESS (PAGE_URL, ROLE_NAME, ID_PERMISSION) 
VALUES ('downloadPlanilhaCadFestaJulina.jsf', 'ROLE_downloadPlanilhaCadFestaJulina', 1)

-- Vizualizar todas as telas Inseridas
SELECT * FROM SEC_PAGE_ACCESS
-- DROP TABLE SEC_PAGE_ACCESS

--Excluindo permissão de tela para determinado ID
-- DELETE FROM SEC_PAGE_ACCESS WHERE PAGE_URL = 'buscaCadFestaJulina.jsf' AND ID_PERMISSION = 1

-- Alterar senha da aplicação
UPDATE CadFestaJulina  set PASSWORD = '21232f297a57a5a743894a0e4a801fc3' WHERE USER_NAME = 'admin'

