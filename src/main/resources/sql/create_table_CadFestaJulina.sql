CREATE TABLE CadFestaJulina
(
  	id               SERIAL NOT NULL,
  	codigoCadastro	 INTEGER UNIQUE NOT NULL,	
	Nome             VARCHAR (255) NOT NULL,
	Idade            INTEGER NULL,
	Celular          VARCHAR (50) NOT NULL,
	Email            VARCHAR (255) NOT NULL,
	Facebook         VARCHAR (255) NULL,
	frequentaIgreja  CHARACTER (1) NOT NULL,
	NomeIgreja       VARCHAR (255) NULL
)

DROP TABLE CadFestaJulina

-- DROP TABLE CadFestaJulina
-- SELECT * FROM CadFestaJulina
