function fbCheckLogin(retry){
	FB.getLoginStatus(function(response) {
		if (response.status === 'connected') {
			fbFillFields();
		} else {
			FB.login(function(response){
				if (response.status === 'connected') {
					fbFillFields();
				} else {
					alert('Não foi possível preencher com o Facebook');
				}
			}, {scope: 'public_profile,email'});
		}
	});
}

function fbFillFields(){
	FB.api('/me?fields=id,email,name,first_name,last_name,age_range,link,gender', function(response) {
		$('#nome,#festajulina\\:nome input').val(response.name);
		$('#email,#festajulina\\:email input').val(response.email);
		$('#email,#festajulina\\:email input').attr('readonly', true);
		$('#facebook,#festajulina\\:facebook input').val('www.facebook.com/' + response.id);
		$('#facebook,#festajulina\\:facebook input').attr('readonly', true);
	});
}

window.fbAsyncInit = function() {
	FB.init({
		appId      : '479578028833416',
		xfbml      : true,
		version    : 'v2.6'
	});
	FB.getLoginStatus(function(response) {
		if (response.status === 'connected') {
			//Verificar se FB_ID ou email estão no BD
			//Caso estejam, redirecionar para página com ID da Festa Julina
			fbFillFields();
		}
	});
};

(function(d, s, id){
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));