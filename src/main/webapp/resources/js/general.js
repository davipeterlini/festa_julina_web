function relogio(id){
	dataAtual = new Date();
	ano = dataAtual.getFullYear().toString();
	mes = dataAtual.getMonth() + 1;
	mes = mes.toString();
	dia = dataAtual.getDate().toString();
	dia = (dia.length == 1 ? "0" + dia : dia);
	mes = (mes.length == 1 ? "0" + mes : mes);
	
	hora = dataAtual.getHours().toString();
	minuto = dataAtual.getMinutes().toString();
	segundo = dataAtual.getSeconds().toString();
	hora = (hora.length == 1 ? "0" + hora : hora);
	minuto = (minuto.length == 1 ? "0" + minuto : minuto);
	segundo = (segundo.length == 1 ? "0" + segundo : segundo);
	
	diaImprimivel = dia + "/" + mes + "/" + ano;
	horaImprimivel = hora + ":" + minuto + ":"
			+ (segundo.length == 1 ? "0" + segundo : segundo);
	document.getElementById(id).innerHTML = diaImprimivel + " - "
			+ horaImprimivel;
	
	setTimeout("relogio('" + id + "')", 1000);
}