$(document).ready(function() {
	
	/**
	 * Bloqueia todos os tipos de caracteres inseridos no campo de numero da nota fiscal exceto números. 
	 */
	$('.numeric').change(function(){
		var convertedValue = Number(this.value);
		this.value = isNaN(convertedValue) ? "" : convertedValue; 
	});
	
});

/**
 * Função que retorna a versão do IE se o browser for Internet Explorer
 */
function isIE () {
	var myNav = navigator.userAgent.toLowerCase();
	return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
}

/**
 * Função para ajustar o colspan da mensagem quando não há datas em uma tabela com filtro
 */
function fixTableHiddenColumns(mode) {
	
	var table = $(".fixTableHiddenColumns");
	
	if (table.find(".ui-datatable-empty-message").length > 0) {
		var hiddenColspan = table.find('.ui-datatable-empty-message').children().attr("colspan");
		table.find('thead tr th').each(function(){
			if($(this).hasClass('ui-helper-hidden')) hiddenColspan--;
		});	
		table.find('.ui-datatable-empty-message').children().attr("colspan", hiddenColspan);
		
		if (isIE() >= 8){
			table.find('table').css("table-layout","fixed");
		}
	}
	
	// Por padrão a tabela inicializa escondida, pois no primeiro acesso se não houver
	// dados, o tempo para renderização faz com que a table seja criada com o colspan original.
	// Sendo assim se for chamada a função no mode init, retiramos esse hack
	if(mode == "init") {
		table.attr("style", "display: block !important");
	}
}

/**
 * Método utilizado para ocultar e exibir determinadas modais. 
 * 
 * @param args
 * 		Argumentos obtidos no evento complete de determinado botão.
 * @param dialogsToCloseOnSuccess
 * 		Modais que serão ocultadas em caso de sucesso.
 * @param dialogsToCloseOnError
 * 		Modais que serão ocultadas em caso de erro.
 */
function hideDialogsOnComplete(args, dialogsToCloseOnSuccess, dialogsToCloseOnError) {
	if(!args.validationFailed) {
		for(index = 0 ; index < dialogsToCloseOnSuccess.length; index++){
			dialogsToCloseOnSuccess[index].hide();
		}
	} else {
		for(index = 0 ; index < dialogsToCloseOnError.length; index++){
			dialogsToCloseOnError[index].hide();
		}
	}
}

/**
 * Método utilizado para exibir determinadas modais.
 * 
 * @param errors
 * 		Lista de erros.
 * @param dialogs
 * 		Modais a serem exibidas
 */
function showDialogsOnComplete(errors, dialogs){
	
	showDialogs = true;
	
	for(index = 0; index < errors.length; index++){
		showDialogs = showDialogs && !errors[index]; 
	}
	
	if(showDialogs) {
		for(index = 0 ; index < dialogs.length; index++){
			dialogs[index].show();
		}
	}
}