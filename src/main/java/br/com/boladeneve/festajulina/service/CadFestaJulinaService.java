package br.com.boladeneve.festajulina.service;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import br.com.boladeneve.festajulina.excepetion.ValidacaoException;
import br.com.boladeneve.festajulina.model.to.CadFestaJulinaTO;

public interface CadFestaJulinaService {
	
	/**
	 * Busca dados de Cadastro de Pessoa da Festa Julina para a Tela da Tabela
	 * de Cadastro
	 * 
	 * @return Valores encontrados na busca no banco de dados
	 * @throws ParseException 
	 * @throws SQLException 
	 */
	List<CadFestaJulinaTO> buscarListaCadFestaJulina() throws ParseException, SQLException;

	/**
	 * Busca Cadastro de Pessoa da Festa Julina para a Confirmação de Cadastro (Pulseira)
	 * Verifica se pessoa ja foi cadastrada na tabela: CadFestaJulina
	 * 
	 * @return Valor encontrados na busca no banco de dados
	 * @throws ParseException 
	 * @throws SQLException 
	 */
	List<CadFestaJulinaTO> buscaCadFestaJulinaPorCodigoDeCadastro(Integer codigoCadastro) throws ParseException, SQLException;
	
	/**
	 * Busca Cadastro de Pessoa da Festa Julina para a recuperar cadastro
	 * Verifica se o nome, ou celular, ou email, ou facebook 
	 * ja foi cadastrada na tabela: CadFestaJulina
	 * 
	 * @return Valor encontrados na busca no banco de dados
	 * @throws ParseException 
	 * @throws SQLException 
	 */
	List<CadFestaJulinaTO> buscaCadFestaJulinaPorCampo(CadFestaJulinaTO cadFestaJulinaTO) throws ParseException, SQLException;

	/**
	 * Insere Cadastro de Pessoa na tabela: CadFestaJulina
	 * 
	 * @param normalizacaoListagemTO
	 * @throws ValidacaoException
	 * 
	 * @throws Exception, SQLException
	 * 
	 */
	Integer insereCadFestaJulina(List<CadFestaJulinaTO>  listCadFestaJulinaTO)
			throws SQLException, Exception;
	
	/**
	 * Busca o ultimo ID cadastrado (afim de mostrar mensagens na tela)
	 * 
	 * @return Ultimo ID encontrado no cadastro de Pessoa
	 * @throws ParseException 
	 * @throws SQLException 
	 */
	/*CadFestaJulinaTO buscaUltimoIDCadFestaJulina() throws ParseException, SQLException;*/

	/**
	 * Busca Cadastro de Pessoa da Festa Julina para a recuperar cadastro
	 * Verifica se o nome ja foi cadastrada na tabela: CadFestaJulina
	 * 
	 * @return Valor encontrados na busca no banco de dados
	 * @throws ParseException 
	 */
	/*List<CadFestaJulinaTO> buscaCadFestaJulinaPorNome(String nome) throws ParseException;*/
	
	/**
	 * Busca Cadastro de Pessoa da Festa Julina para a recuperar cadastro
	 * Verifica se o email ja foi cadastrada na tabela: CadFestaJulina
	 * 
	 * @return Valor encontrados na busca no banco de dados
	 * @throws ParseException 
	 */
	/*List<CadFestaJulinaTO> buscaCadFestaJulinaPorEmail(String email) throws ParseException;*/
	
	/**
	 * Busca Cadastro de Pessoa da Festa Julina para a recuperar cadastro
	 * Verifica se o celular ja foi cadastrada na tabela: CadFestaJulina
	 * 
	 * @return Valor encontrados na busca no banco de dados
	 * @throws ParseException 
	 */
	/*List<CadFestaJulinaTO> buscaCadFestaJulinaPorCelular(String celular) throws ParseException;*/
	
	/**
	 * Busca Cadastro de Pessoa da Festa Julina para a recuperar cadastro
	 * Verifica se o facebook ja foi cadastrada na tabela: CadFestaJulina
	 * 
	 * @return Valor encontrados na busca no banco de dados
	 * @throws ParseException 
	 */
	/*List<CadFestaJulinaTO> buscaCadFestaJulinaPorFacebook(String facebook) throws ParseException;*/


}
