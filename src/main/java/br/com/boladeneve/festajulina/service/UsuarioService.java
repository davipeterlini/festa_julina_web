package br.com.boladeneve.festajulina.service;

import java.text.ParseException;
import java.util.List;

import br.com.boladeneve.festajulina.model.to.UsuarioTO;

public interface UsuarioService {

    /**
     * Persiste determinada Usuario no banco de dados.
     * 
     * @param Usuario
     *            Objeto do tipo {@link UsuarioTO} a ser persistido.
     * @return Objeto do tipo {@link UsuarioTO} persistido.
     * @throws Exception
     */
    public UsuarioTO salvarUsuario(UsuarioTO usuarioTO) throws ParseException, Exception;

    /**
     * Atualiza determinada Usuario no banco de dados.
     * 
     * @param Usuario
     *            Objeto do tipo {@link UsuarioTO} a ser atualizado.
     * @return Objeto do tipo {@link UsuarioTO} atualizado.
     */
    public UsuarioTO atualizarUsuario(UsuarioTO usuarioTO) throws Exception;

    /**
     * Busca determinada Usuario pelo seu identificador.
     * 
     * @param idUsuario
     *            Identificador da Usuario a ser buscada.
     * @return Objeto do tipo {@link UsuarioTO} com as informações da Usuario
     *         buscada.
     */
    public UsuarioTO buscarUsuarioPorId(Long idUsuario) throws ParseException;

    /**
     * Busca todos Usuario.
     * 
     * @return List do tipo {@link UsuarioTO} com as informações da Usuario.
     */
    public List<UsuarioTO> buscarUsuarioTodos() throws ParseException;

}