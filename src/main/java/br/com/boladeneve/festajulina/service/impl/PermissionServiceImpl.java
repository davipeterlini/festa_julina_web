package br.com.boladeneve.festajulina.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.boladeneve.festajulina.dao.PermissionDAO;
import br.com.boladeneve.festajulina.service.PermissionService;

/**
 * The Class PermissionServiceImpl.
 */
@Service
@Transactional
public class PermissionServiceImpl implements PermissionService {

	@Autowired
	private PermissionDAO permissionDAO;

	/**
	 * Find permission name by username
	 */
    @Override
    public String findPermissionByUsername(String username) {
        return permissionDAO.findByUsername(username);
    }

}
