package br.com.boladeneve.festajulina.service.impl;

import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.boladeneve.festajulina.dao.PermissaoDAO;
import br.com.boladeneve.festajulina.model.converter.PermissaoConverter;
import br.com.boladeneve.festajulina.model.entity.Permissao;
import br.com.boladeneve.festajulina.model.to.PermissaoTO;
import br.com.boladeneve.festajulina.service.PermissaoService;

@Service
@Transactional
public class PermissaoServiceImpl implements PermissaoService {

    private final Logger LOGGER = LoggerFactory.getLogger(PermissaoService.class);

    @Autowired
    private PermissaoDAO permissaoDAO;

    @Override
    public PermissaoTO salvarPermissao(PermissaoTO permissaoTO) throws ParseException, Exception {

        return permissaoTO;
    }

    @Override
    public PermissaoTO atualizarPermissao(PermissaoTO permissaoTO) throws Exception {

        return permissaoTO;
    }

    @Override
    public PermissaoTO buscarPermissaoPorId(final Long idPermissao) throws ParseException {

        LOGGER.info("INICIO SERVIÇO - buscaPermissaoPorId. Parametros: {}", idPermissao);

        final Permissao Permissao = permissaoDAO.buscarPorId(idPermissao);
        final PermissaoTO PermissaoTO = PermissaoConverter.permissaoEntityParaPermissaoTO(Permissao);

        LOGGER.info("FIM SERVIÇO - buscaPermissaoPorId. Parametros: {}", PermissaoTO);

        return PermissaoTO;
    }

    @Override
    public List<PermissaoTO> buscarPermissaoTodos() throws ParseException {

        LOGGER.info("INICIO SERVIÇO - buscaPermissaoTodos.");

        final List<Permissao> listPermissao = permissaoDAO.buscarTodos();
        final List<PermissaoTO> listPermissaoTO = PermissaoConverter.permissaoEntityListParaPermissaoTOList(listPermissao);

        LOGGER.info("FIM SERVIÇO - buscaPermissaoTodos. Parametros: {}", listPermissaoTO);

        return listPermissaoTO;
    }


}