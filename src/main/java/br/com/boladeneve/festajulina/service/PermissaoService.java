package br.com.boladeneve.festajulina.service;

import java.text.ParseException;
import java.util.List;

import br.com.boladeneve.festajulina.model.to.PermissaoTO;

public interface PermissaoService {

    /**
     * Persiste determinada Permissao no banco de dados.
     * 
     * @param Permissao
     *            Objeto do tipo {@link PermissaoTO} a ser persistido.
     * @return Objeto do tipo {@link PermissaoTO} persistido.
     * @throws Exception
     */
    public PermissaoTO salvarPermissao(PermissaoTO PermissaoTO) throws ParseException, Exception;

    /**
     * Atualiza determinada Permissao no banco de dados.
     * 
     * @param Permissao
     *            Objeto do tipo {@link PermissaoTO} a ser atualizado.
     * @return Objeto do tipo {@link PermissaoTO} atualizado.
     */
    public PermissaoTO atualizarPermissao(PermissaoTO PermissaoTO) throws Exception;

    /**
     * Busca determinada Permissao pelo seu identificador.
     * 
     * @param idPermissao
     *            Identificador da Permissao a ser buscada.
     * @return Objeto do tipo {@link PermissaoTO} com as informações da Permissao
     *         buscada.
     */
    public PermissaoTO buscarPermissaoPorId(Long idPermissao) throws ParseException;

    /**
     * Busca todos Permissao.
     * 
     * @return List do tipo {@link PermissaoTO} com as informações da Permissao.
     */
    public List<PermissaoTO> buscarPermissaoTodos() throws ParseException;

}