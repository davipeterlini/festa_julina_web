package br.com.boladeneve.festajulina.service.impl;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.faces.convert.ConverterException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.boladeneve.festajulina.constantes.CadFestaJulinaEnum;
import br.com.boladeneve.festajulina.dao.CadFestaJulinaDAO;
import br.com.boladeneve.festajulina.excepetion.ValidacaoException;
import br.com.boladeneve.festajulina.model.converter.CadFestaJulinaConverter;
import br.com.boladeneve.festajulina.model.to.CadFestaJulinaTO;
import br.com.boladeneve.festajulina.service.CadFestaJulinaService;

@Service
public class CadFestaJulinaServiceImpl implements CadFestaJulinaService {

	private static final int randomCod = 10000000;

	private final Logger LOGGER = LoggerFactory
			.getLogger(CadFestaJulinaServiceImpl.class);

	@Autowired
	private CadFestaJulinaDAO cadFestaJulinaDAO;
	
	@Override
	public List<CadFestaJulinaTO> buscarListaCadFestaJulina()
			throws ParseException, SQLException {
		LOGGER.info("INICIO SERVICO - buscarListaCadFestaJulina");

		List<Object[]> listObjectCadFestaJulinaTO = cadFestaJulinaDAO.buscarListaCadFestaJulina();

		List<CadFestaJulinaTO> listCadFestaJulinaTO = new ArrayList<CadFestaJulinaTO>();
		
		try {
			listCadFestaJulinaTO = CadFestaJulinaConverter.cadFestaJulinaObjectListParaCadFestaJulinaTOList(listObjectCadFestaJulinaTO);
		} catch (ParseException e) {
			throw new ConverterException("message_conversao_error", e);
		}

		LOGGER.info("FIM SERVICO - buscarListaCadFestaJulina");

		return listCadFestaJulinaTO;
	}

	@Override
	public List<CadFestaJulinaTO> buscaCadFestaJulinaPorCodigoDeCadastro(Integer codigoCadastro)
			throws ParseException, SQLException {
		LOGGER.info("INICIO SERVICO - buscaCadFestaJulinaPorCodigoDeCadastro");

		List<Object[]> listObjectCadFestaJulinaTO = cadFestaJulinaDAO.buscaCadFestaJulinaPorCodigoDeCadastro(codigoCadastro);

		List<CadFestaJulinaTO> listCadFestaJulinaTO = new ArrayList<CadFestaJulinaTO>();
		
		try {
			listCadFestaJulinaTO = CadFestaJulinaConverter.cadFestaJulinaObjectListParaCadFestaJulinaTOList(listObjectCadFestaJulinaTO);
		} catch (ParseException e) {
			throw new ConverterException("message_conversao_error", e);
		}

		LOGGER.info("FIM SERVICO - buscaCadFestaJulinaPorCodigoDeCadastro");

		return listCadFestaJulinaTO;
	}
	
	@Override
	public List<CadFestaJulinaTO> buscaCadFestaJulinaPorCampo(CadFestaJulinaTO cadFestaJulinaTO) throws ParseException, SQLException {
		LOGGER.info("INICIO SERVICO - buscaCadFestaJulinaPorNome");

		List<Object[]> listObjectCadFestaJulinaTO = cadFestaJulinaDAO.buscaCadFestaJulinaPorCampo(cadFestaJulinaTO);

		List<CadFestaJulinaTO> listCadFestaJulinaTO = new ArrayList<CadFestaJulinaTO>();
		
		try {
			listCadFestaJulinaTO = CadFestaJulinaConverter.cadFestaJulinaObjectListParaCadFestaJulinaTOList(listObjectCadFestaJulinaTO);
		} catch (ParseException e) {
			throw new ConverterException("message_conversao_error", e);
		}

		LOGGER.info("FIM SERVICO - buscaCadFestaJulinaPorNome");

		return listCadFestaJulinaTO;
	}

	@Transactional
	@Override
	public Integer insereCadFestaJulina(List<CadFestaJulinaTO> listCadFestaJulinaTO)
			throws ValidacaoException, SQLException{
		
		LOGGER.info("INICIO SERVICO - insereCadFestaJulina");
		
		Integer insereDadoBanco = null;
		
		for (CadFestaJulinaTO cadFestaJulinaTO : listCadFestaJulinaTO) {
			
			// Grava Flag Frequenta Igreja
			if (cadFestaJulinaTO.getFrequentaIgreja().equals("sim")) {
				cadFestaJulinaTO.setFrequentaIgreja(CadFestaJulinaEnum.SIM.toString());
			} else {
				cadFestaJulinaTO.setFrequentaIgreja(CadFestaJulinaEnum.NAO.toString());
			}			
			
			// Tratamento Prévio para possível cadastro através do facebook
			String fbString = cadFestaJulinaTO.getFacebook();
			if (!fbString.contains("facebook.com")) {
				cadFestaJulinaTO.setFacebook("www.facebook.com/".concat(fbString));
			}
			
			// Gera código aleatório
			Random gerador = new Random();
			cadFestaJulinaTO.setCodigoCadastro(gerador.nextInt(randomCod));
			
			// Verifica se código aleatório ja existe no banco
			List<Object[]> listObjectsVerificaItemCadFestaJulina = cadFestaJulinaDAO.buscaCadFestaJulinaPorCodigoDeCadastro(cadFestaJulinaTO.getCodigoCadastro());
			
			if (!listObjectsVerificaItemCadFestaJulina.isEmpty()) {
				while (!listObjectsVerificaItemCadFestaJulina.isEmpty()) {			
					cadFestaJulinaTO.setCodigoCadastro(gerador.nextInt(randomCod));
					
					listObjectsVerificaItemCadFestaJulina = cadFestaJulinaDAO.buscaCadFestaJulinaPorCodigoDeCadastro(cadFestaJulinaTO.getCodigoCadastro());
				}
				insereDadoBanco = insereDadoBanco(insereDadoBanco, cadFestaJulinaTO);
			} else {
				insereDadoBanco = insereDadoBanco(insereDadoBanco, cadFestaJulinaTO);
			}
		}
		
		LOGGER.info("FIM SERVICO - insereCadFestaJulina");
		
		return insereDadoBanco;
	}

	private Integer insereDadoBanco(Integer insereDadoBanco,
			CadFestaJulinaTO cadFestaJulinaTO) throws ValidacaoException {
		try {
			insereDadoBanco = cadFestaJulinaDAO.insereCadFestaJulina(cadFestaJulinaTO);
		} catch (ValidacaoException e) {
			throw new ValidacaoException("message_insersao_error");
		}catch (SQLException e) {
			throw new ValidacaoException("message_insersao_sql_error");
		}
		return insereDadoBanco;
	}

	/*public CadFestaJulinaTO buscaUltimoIDCadFestaJulina() throws ParseException, SQLException {

		LOGGER.info("INICIO SERVICO - buscaUltimoIDCadFestaJulina");

		List<Object[]> listObjectsUltimoIdCadFestaJulian = cadFestaJulinaDAO.buscaUltimoIDCadFestaJulina();
		
		List<CadFestaJulinaTO> listCadFestaJulinaTO = new ArrayList<CadFestaJulinaTO>();
		
		try {
			listCadFestaJulinaTO = CadFestaJulinaConverter.cadFestaJulinaObjectListParaCadFestaJulinaTOList(listObjectsUltimoIdCadFestaJulian);
		} catch (ParseException e) {
			throw new ConverterException("message_conversao_error", e);
		}

		LOGGER.info("FIM SERVICO - buscaUltimoIDCadFestaJulina");

		return listCadFestaJulinaTO.get(0);

	}
	
	@Override
	public List<CadFestaJulinaTO> buscaCadFestaJulinaPorNome(String nome) throws ParseException {
		LOGGER.info("INICIO SERVICO - buscaCadFestaJulinaPorNome");

		List<Object[]> listObjectCadFestaJulinaTO = cadFestaJulinaDAO.buscaCadFestaJulinaPorNome(nome);

		List<CadFestaJulinaTO> listCadFestaJulinaTO = CadFestaJulinaConverter.cadFestaJulinaObjectListParaCadFestaJulinaTOList(listObjectCadFestaJulinaTO);

		LOGGER.info("FIM SERVICO - buscaCadFestaJulinaPorNome");

		return listCadFestaJulinaTO;
	}
	
	@Override
	public List<CadFestaJulinaTO> buscaCadFestaJulinaPorEmail(String email) throws ParseException {
		LOGGER.info("INICIO SERVICO - buscaCadFestaJulinaPorEmail");

		List<Object[]> listObjectCadFestaJulinaTO = cadFestaJulinaDAO.buscaCadFestaJulinaPorEmail(email);

		List<CadFestaJulinaTO> listCadFestaJulinaTO = CadFestaJulinaConverter.cadFestaJulinaObjectListParaCadFestaJulinaTOList(listObjectCadFestaJulinaTO);

		LOGGER.info("FIM SERVICO - buscaCadFestaJulinaPorEmail");

		return listCadFestaJulinaTO;
	}
	
	@Override
	public List<CadFestaJulinaTO> buscaCadFestaJulinaPorCelular(String celular) throws ParseException {
		LOGGER.info("INICIO SERVICO - buscaCadFestaJulinaPorCelular");

		List<Object[]> listObjectCadFestaJulinaTO = cadFestaJulinaDAO.buscaCadFestaJulinaPorCelular(celular);

		List<CadFestaJulinaTO> listCadFestaJulinaTO = CadFestaJulinaConverter.cadFestaJulinaObjectListParaCadFestaJulinaTOList(listObjectCadFestaJulinaTO);

		LOGGER.info("FIM SERVICO - buscaCadFestaJulinaPorCelular");

		return listCadFestaJulinaTO;
	}
	
	@Override
	public List<CadFestaJulinaTO> buscaCadFestaJulinaPorFacebook(String facebook) throws ParseException {
		LOGGER.info("INICIO SERVICO - buscaCadFestaJulinaPorFacebook");

		List<Object[]> listObjectCadFestaJulinaTO = cadFestaJulinaDAO.buscaCadFestaJulinaPorFacebook(celular);

		List<CadFestaJulinaTO> listCadFestaJulinaTO = CadFestaJulinaConverter.cadFestaJulinaObjectListParaCadFestaJulinaTOList(listObjectCadFestaJulinaTO);

		LOGGER.info("FIM SERVICO - buscaCadFestaJulinaPorFacebook");

		return listCadFestaJulinaTO;
	}*/
}
