package br.com.boladeneve.festajulina.service.impl;

import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.boladeneve.festajulina.dao.UsuarioDAO;
import br.com.boladeneve.festajulina.model.converter.UsuarioConverter;
import br.com.boladeneve.festajulina.model.entity.Usuario;
import br.com.boladeneve.festajulina.model.to.UsuarioTO;
import br.com.boladeneve.festajulina.service.UsuarioService;

@Service
@Transactional
public class UsuarioServiceImpl implements UsuarioService {

    private final Logger LOGGER = LoggerFactory.getLogger(UsuarioService.class);

    @Autowired
    private UsuarioDAO usuarioDAO;

    @Override
    public UsuarioTO salvarUsuario(UsuarioTO usuarioTO) throws ParseException, Exception {

        return usuarioTO;
    }

    @Override
    public UsuarioTO atualizarUsuario(UsuarioTO usuarioTO) throws Exception {

        return usuarioTO;
    }

    @Override
    public UsuarioTO buscarUsuarioPorId(final Long idUsuario) throws ParseException {

        LOGGER.info("INICIO SERVIÇO - buscaUsuarioPorId. Parametros: {}", idUsuario);

        final Usuario usuario = usuarioDAO.buscarPorId(idUsuario);
        final UsuarioTO UsuarioTO = UsuarioConverter.UsuarioEntityParaUsuarioTO(usuario);

        LOGGER.info("FIM SERVIÇO - buscaUsuarioPorId. Parametros: {}", UsuarioTO);

        return UsuarioTO;
    }

    @Override
    public List<UsuarioTO> buscarUsuarioTodos() throws ParseException {

        LOGGER.info("INICIO SERVIÇO - buscaUsuarioTodos.");

        final List<Usuario> listUsuario = usuarioDAO.buscarTodos();
        final List<UsuarioTO> listUsuarioTO = UsuarioConverter.UsuarioEntityListParaUsuarioTOList(listUsuario);

        LOGGER.info("FIM SERVIÇO - buscaUsuarioTodos. Parametros: {}", listUsuarioTO);

        return listUsuarioTO;
    }

}