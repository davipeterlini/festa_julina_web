package br.com.boladeneve.festajulina.service;


/**
 * The Interface for services related to Permission.
 */
public interface PermissionService {

	/**
	 * Find permission by username.
	 *
	 * @param userName
	 * @return permisson
	 */
	String findPermissionByUsername(String username);
}
