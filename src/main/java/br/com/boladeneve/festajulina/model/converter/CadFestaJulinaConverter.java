package br.com.boladeneve.festajulina.model.converter;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.com.boladeneve.festajulina.model.to.CadFestaJulinaTO;
import br.com.boladeneve.festajulina.util.ValidacaoUtil;

public class CadFestaJulinaConverter {

	/**
	 * Metodo que converte uma lista de query Object em cadFestaJulinaTO. Caso
	 * a entidade venha nula ou vazia retorna uma nova listagem.
	 * 
	 * @param listObjectCadFestaJulinaTO
	 * 			A lista de Entidades da Cadastro da Festa Julina
	 * @return A Lista de TO's CadFestaJulina
	 * @throws ParseException
	 */
	public static List<CadFestaJulinaTO> cadFestaJulinaObjectListParaCadFestaJulinaTOList(
			final List<Object[]> listObjectCadFestaJulinaTO) throws ParseException {

		final List<CadFestaJulinaTO> listaCadFestaJulinaTO = new ArrayList<CadFestaJulinaTO>();

		if (!ValidacaoUtil.isEmpty(listObjectCadFestaJulinaTO)) {
			for (final Object[] objectCadFestaJulina : listObjectCadFestaJulinaTO) {
				listaCadFestaJulinaTO.add(CadFestaJulinaConverter.cadFestaJulinaObjectParaCadFestaJulinaTO(objectCadFestaJulina));
			}
		}
		return listaCadFestaJulinaTO;
	}
	
    /**
     * Metodo que converte um resultado de query Object em CadFestaJulinaTO
     * Caso a entidade venha nula ou vazia retorna um novo TO CadFestaJUlina
     * 
     * @param objectCadFestaJulina
     *            O objeto a ser convertido
     * @return TO de CadFestaJulinaTO
     * @throws ParseException
     *             Erro ao converter uma data.
     */
    public static CadFestaJulinaTO cadFestaJulinaObjectParaCadFestaJulinaTO(
            final Object[] objectCadFestaJulina) throws ParseException {

        final CadFestaJulinaTO cadFestaJulinaTO = new CadFestaJulinaTO();

        if (!ValidacaoUtil.isEmpty(objectCadFestaJulina)) {    
        	int i = 0;
        	
        	cadFestaJulinaTO.setId(Integer.valueOf(objectCadFestaJulina[i++].toString()));
        	cadFestaJulinaTO.setCodigoCadastro(Integer.valueOf(objectCadFestaJulina[i++].toString()));
        	cadFestaJulinaTO.setNome(objectCadFestaJulina[i++].toString());
        	cadFestaJulinaTO.setIdade(Integer.valueOf(objectCadFestaJulina[i++].toString()));
        	cadFestaJulinaTO.setCelular(objectCadFestaJulina[i++].toString());
        	cadFestaJulinaTO.setEmail(objectCadFestaJulina[i++].toString());
        	cadFestaJulinaTO.setFacebook(objectCadFestaJulina[i++].toString());
        	cadFestaJulinaTO.setFrequentaIgreja(objectCadFestaJulina[i++].toString()); 
        	cadFestaJulinaTO.setNomeIgreja(objectCadFestaJulina[i++].toString());
        	
        }
        
        return cadFestaJulinaTO;
    }
}
