package br.com.boladeneve.festajulina.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.boladeneve.festajulina.table.TableMap;

@Entity
@Table(name = TableMap.CADFESTAJULINA)
public class CadFestaJulinaEntity implements Serializable {

	private static final long serialVersionUID = 1L;
		
	@Id
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "codigoCadastro")
	private Integer codigoCadastro;

	@Column(name = "nome")
	private String nome;
	
	@Column(name = "idade")
	private Integer idade;

	@Column(name = "celular")
	private String celular;

	@Column(name = "email")
	private String email;

	@Column(name = "facebook")
	private String facebook;

	@Column(name = "frequentaigreja")
	private String frequentaIgreja;

	
	@Column(name = "nomeigreja")
	private String nomeIgreja;


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getCodigoCadastro() {
		return codigoCadastro;
	}


	public void setCodigoCadastro(Integer codigoCadastro) {
		this.codigoCadastro = codigoCadastro;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public Integer getIdade() {
		return idade;
	}


	public void setIdade(Integer idade) {
		this.idade = idade;
	}


	public String getCelular() {
		return celular;
	}


	public void setCelular(String celular) {
		this.celular = celular;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getFacebook() {
		return facebook;
	}


	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}


	public String getFrequentaIgreja() {
		return frequentaIgreja;
	}


	public void setFrequentaIgreja(String frequentaIgreja) {
		this.frequentaIgreja = frequentaIgreja;
	}


	public String getNomeIgreja() {
		return nomeIgreja;
	}


	public void setNomeIgreja(String nomeIgreja) {
		this.nomeIgreja = nomeIgreja;
	}
	
	
}