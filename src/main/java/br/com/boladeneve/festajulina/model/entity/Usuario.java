package br.com.boladeneve.festajulina.model.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the SEC_USER database table.
 * 
 */
@Entity
@Table(name = "SEC_USER")
@NamedQuery(name = "Usuario.Todos", query = "SELECT s FROM Usuario s")
public class Usuario implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @Column(name = "IS_ENABLED")
    private String isEnabled;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "USER_NAME")
    private String userName;

    // bi-directional many-to-one association to SecUserPermission
    @OneToMany(mappedBy = "secUser")
    private List<SecUserPermission> secUserPermissions;

    public Usuario() {
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIsEnabled() {
        return this.isEnabled;
    }

    public void setIsEnabled(String isEnabled) {
        this.isEnabled = isEnabled;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<SecUserPermission> getSecUserPermissions() {
        return this.secUserPermissions;
    }

    public void setSecUserPermissions(List<SecUserPermission> secUserPermissions) {
        this.secUserPermissions = secUserPermissions;
    }

    public SecUserPermission addSecUserPermission(SecUserPermission secUserPermission) {
        getSecUserPermissions().add(secUserPermission);
        secUserPermission.setSecUser(this);

        return secUserPermission;
    }

    public SecUserPermission removeSecUserPermission(SecUserPermission secUserPermission) {
        getSecUserPermissions().remove(secUserPermission);
        secUserPermission.setSecUser(null);

        return secUserPermission;
    }

}