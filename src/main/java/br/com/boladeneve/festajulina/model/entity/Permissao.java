package br.com.boladeneve.festajulina.model.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the SEC_PERMISSION database table.
 * 
 */
@Entity
@Table(name="SEC_PERMISSION")
@NamedQuery(name="Permissao.todos", query="SELECT s FROM Permissao s")
public class Permissao implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID", unique=true, nullable=false, precision=19)
	private long id;

	@Column(name="NAME", length=20)
	private String name;

	public Permissao() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}