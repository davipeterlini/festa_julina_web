package br.com.boladeneve.festajulina.model.to;

import java.io.Serializable;

public class LoginTO implements Serializable {

	private static final long serialVersionUID = -3550670601835510015L;

	private String usuario;

	private String permissao;

	private Long idPermissao;

	public LoginTO() {
	}

	public LoginTO(String usuario, String permissao, Long idPermissao) {
		super();
		this.usuario = usuario;
		this.permissao = permissao;
		this.idPermissao = idPermissao;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPermissao() {
		return permissao;
	}

	public void setPermissao(String permissao) {
		this.permissao = permissao;
	}

	public Long getIdPermissao() {
		return idPermissao;
	}

	public void setIdPermissao(Long idPermissao) {
		this.idPermissao = idPermissao;
	}

}
