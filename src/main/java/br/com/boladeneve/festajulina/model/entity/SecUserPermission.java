package br.com.boladeneve.festajulina.model.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the SEC_USER_PERMISSION database table.
 * 
 */
@Entity
@Table(name="SEC_USER_PERMISSION")
@NamedQuery(name="SecUserPermission.findAll", query="SELECT s FROM SecUserPermission s")
public class SecUserPermission implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private long id;

	@Column(name="ID_PERMISSION")
	private java.math.BigDecimal idPermission;

	//bi-directional many-to-one association to SecUser
	@ManyToOne
	@JoinColumn(name="ID_USER")
	private Usuario secUser;

	public SecUserPermission() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public java.math.BigDecimal getIdPermission() {
		return this.idPermission;
	}

	public void setIdPermission(java.math.BigDecimal idPermission) {
		this.idPermission = idPermission;
	}

	public Usuario getSecUser() {
		return this.secUser;
	}

	public void setSecUser(Usuario secUser) {
		this.secUser = secUser;
	}

}