package br.com.boladeneve.festajulina.model.converter;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.com.boladeneve.festajulina.model.entity.Usuario;
import br.com.boladeneve.festajulina.model.to.UsuarioTO;
import br.com.boladeneve.festajulina.util.ValidacaoUtil;

public class UsuarioConverter {

    /**
     * Metodo que converte uma entidade de Usuario em um TO de UsuarioTO Caso a
     * entidade venha nula ou vazia retorna um novo TO UsuarioTO
     * 
     * @param UsuarioEntity
     *            A entidade de Usuario
     * @return TO de UsuarioTO
     * @throws ParseException
     */
    public static UsuarioTO UsuarioEntityParaUsuarioTO(Usuario UsuarioEntity) throws ParseException {

        UsuarioTO UsuarioTO = new UsuarioTO();

        if (!ValidacaoUtil.isEmpty(UsuarioEntity)) {
            UsuarioTO.setId(UsuarioEntity.getId());
            UsuarioTO.setIsEnabled(UsuarioEntity.getIsEnabled());
            UsuarioTO.setPassword(UsuarioEntity.getPassword());
            UsuarioTO.setSecUserPermissions(UsuarioEntity.getSecUserPermissions());
            UsuarioTO.setUserName(UsuarioEntity.getUserName());
        }

        return UsuarioTO;
    }

    /**
     * Metodo que converte uma lista de entidades de Usuario em uma Lista de
     * TO's de UsuarioTO Caso a lista esteja vazia ou nula torna uma nova lista
     * de TO UsuarioTO
     * 
     * @param listUsuarioEntity
     *            A lista de Entidades de Usuario
     * @return A lista de TO's UsuarioTO
     * @throws ParseException
     */
    public static List<UsuarioTO> UsuarioEntityListParaUsuarioTOList(List<Usuario> listUsuarioEntity)
            throws ParseException {

        List<UsuarioTO> listUsuarioTO = new ArrayList<UsuarioTO>();

        if (!ValidacaoUtil.isEmpty(listUsuarioEntity)) {
            for (final Usuario UsuarioEntity : listUsuarioEntity) {
                UsuarioTO UsuarioTO = new UsuarioTO();

                UsuarioTO.setId(UsuarioEntity.getId());
                UsuarioTO.setIsEnabled(UsuarioEntity.getIsEnabled());
                UsuarioTO.setPassword(UsuarioEntity.getPassword());
                UsuarioTO.setSecUserPermissions(UsuarioEntity.getSecUserPermissions());
                UsuarioTO.setUserName(UsuarioEntity.getUserName());

                listUsuarioTO.add(UsuarioTO);
            }
        }
        return listUsuarioTO;
    }

    /**
     * Metodo que converte um UsuarioTO em uma entidade de Usuario Caso o TO
     * venha nulo ou vaziu retorna uma nova entidade Usuario
     * 
     * @param UsuarioTO
     *            O TO de Usuario
     * @return Entity de Parametro Contabilidade
     * @throws ParseException
     */
    public static Usuario UsuarioTOParaUsuarioEntity(UsuarioTO UsuarioTO) throws ParseException {

        Usuario usuario = new Usuario();
        if (!ValidacaoUtil.isEmpty(UsuarioTO)) {
            usuario.setId(UsuarioTO.getId());
            usuario.setIsEnabled(UsuarioTO.getIsEnabled());
            usuario.setPassword(UsuarioTO.getPassword());
            usuario.setSecUserPermissions(UsuarioTO.getSecUserPermissions());
            usuario.setUserName(UsuarioTO.getUserName());
        }

        return usuario;
    }

    /**
     * Metodo que converte uma lista de TO de UsuarioTO em uma Lista de
     * entidades de Usuario Caso a lista esteja vazia ou nula retorna uma nova
     * lista de Entidades Usuario
     * 
     * @param listUsuarioTO
     *            A lista de TO's de UsuarioTO
     * @return A lista de Entidades Usuario
     * @throws ParseException
     */
    public static List<Usuario> UsuarioTOListParaUsuarioEntityList(List<UsuarioTO> listUsuarioTO) throws ParseException {

        List<Usuario> listUsuario = new ArrayList<Usuario>();

        if (!ValidacaoUtil.isEmpty(listUsuarioTO)) {
            for (final UsuarioTO UsuarioTO : listUsuarioTO) {
                Usuario usuario = new Usuario();
                usuario.setId(UsuarioTO.getId());
                usuario.setIsEnabled(UsuarioTO.getIsEnabled());
                usuario.setPassword(UsuarioTO.getPassword());
                usuario.setSecUserPermissions(UsuarioTO.getSecUserPermissions());
                usuario.setUserName(UsuarioTO.getUserName());

                listUsuario.add(usuario);
            }
        }

        return listUsuario;
    }
}