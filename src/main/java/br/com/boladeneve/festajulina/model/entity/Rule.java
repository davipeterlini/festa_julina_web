package br.com.boladeneve.festajulina.model.entity;

import java.io.Serializable;

public class Rule implements Serializable {

	private static final long serialVersionUID = -5233874023627097958L;

	private Long id;

	private String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
