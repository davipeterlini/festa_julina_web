package br.com.boladeneve.festajulina.model.to;

import java.io.Serializable;
import java.util.List;

import br.com.boladeneve.festajulina.model.entity.SecUserPermission;

public class UsuarioTO implements Serializable {

    private static final long serialVersionUID = 1L;


    private long id;


    private String isEnabled;


    private String password;


    private String userName;
    
    private List<SecUserPermission> secUserPermissions;


    public UsuarioTO() {
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIsEnabled() {
        return this.isEnabled;
    }

    public void setIsEnabled(String isEnabled) {
        this.isEnabled = isEnabled;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<SecUserPermission> getSecUserPermissions() {
        return this.secUserPermissions;
    }

    public void setSecUserPermissions(List<SecUserPermission> secUserPermissions) {
        this.secUserPermissions = secUserPermissions;
    }
}