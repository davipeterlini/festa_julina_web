package br.com.boladeneve.festajulina.model.to;


public class ObrigatoriedadeCampoTO {

	private String nomeCampo;

	private String valorCampo;

	private Boolean artigoMasc;

	public ObrigatoriedadeCampoTO() {
	}

	public ObrigatoriedadeCampoTO(final String nomeCampo, final String valorCampo, final Boolean artigoMasc) {
		super();
		this.nomeCampo = nomeCampo;
		this.valorCampo = valorCampo;
		this.artigoMasc = artigoMasc;
	}

    public String getNomeCampo() {
        return nomeCampo;
    }

    public void setNomeCampo(final String nomeCampo) {
        this.nomeCampo = nomeCampo;
    }

    public String getValorCampo() {
        return valorCampo;
    }

    public void setValorCampo(final String valorCampo) {
        this.valorCampo = valorCampo;
    }

    public Boolean getArtigoMasc() {
        return artigoMasc;
    }

    public void setArtigoMasc(final Boolean artigoMasc) {
        this.artigoMasc = artigoMasc;
    }
}
