package br.com.boladeneve.festajulina.model.to;

import java.io.Serializable;

public class CadFestaJulinaTO implements Serializable {

	private static final long serialVersionUID = 959753685259198483L;
	
	private Integer id;
	
	private Integer codigoCadastro;
	
	private String nome;
	
	private Integer idade;
	
	private String celular;
	
	private String email;
	
	private String facebook;
	
	private String frequentaIgreja;
	
	private String nomeIgreja;
	

	public CadFestaJulinaTO() {
		super();
	}


	public CadFestaJulinaTO(Integer id, Integer codigoCadastro, String nome,
			Integer idade, String celular, String email, String facebook,
			String frequentaIgreja, String nomeIgreja) {
		super();
		this.id = id;
		this.codigoCadastro = codigoCadastro;
		this.nome = nome;
		this.idade = idade;
		this.celular = celular;
		this.email = email;
		this.facebook = facebook;
		this.frequentaIgreja = frequentaIgreja;
		this.nomeIgreja = nomeIgreja;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getCodigoCadastro() {
		return codigoCadastro;
	}


	public void setCodigoCadastro(Integer codigoCadastro) {
		this.codigoCadastro = codigoCadastro;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public Integer getIdade() {
		return idade;
	}


	public void setIdade(Integer idade) {
		this.idade = idade;
	}


	public String getCelular() {
		return celular;
	}


	public void setCelular(String celular) {
		this.celular = celular;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getFacebook() {
		return facebook;
	}


	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}


	public String getFrequentaIgreja() {
		return frequentaIgreja;
	}


	public void setFrequentaIgreja(String frequentaIgreja) {
		this.frequentaIgreja = frequentaIgreja;
	}


	public String getNomeIgreja() {
		return nomeIgreja;
	}


	public void setNomeIgreja(String nomeIgreja) {
		this.nomeIgreja = nomeIgreja;
	}



}
