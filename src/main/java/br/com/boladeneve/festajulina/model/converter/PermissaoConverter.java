package br.com.boladeneve.festajulina.model.converter;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.com.boladeneve.festajulina.model.entity.Permissao;
import br.com.boladeneve.festajulina.model.to.PermissaoTO;
import br.com.boladeneve.festajulina.util.ValidacaoUtil;

public class PermissaoConverter {

    /**
     * Metodo que converte uma entidade de Permissao em um TO de PermissaoTO Caso a
     * entidade venha nula ou vazia retorna um novo TO PermissaoTO
     * 
     * @param permissaoEntity
     *            A entidade de Permissao
     * @return TO de PermissaoTO
     * @throws ParseException
     */
    public static PermissaoTO permissaoEntityParaPermissaoTO(Permissao permissaoEntity) throws ParseException {

        PermissaoTO permissaoTO = new PermissaoTO();

        if (!ValidacaoUtil.isEmpty(permissaoEntity)) {
            permissaoTO.setId(permissaoEntity.getId());
            permissaoTO.setName(permissaoTO.getName());
        }

        return permissaoTO;
    }

    /**
     * Metodo que converte uma lista de entidades de Permissao em uma Lista de
     * TO's de PermissaoTO Caso a lista esteja vazia ou nula torna uma nova lista
     * de TO PermissaoTO
     * 
     * @param listpermissaoEntity
     *            A lista de Entidades de Permissao
     * @return A lista de TO's PermissaoTO
     * @throws ParseException
     */
    public static List<PermissaoTO> permissaoEntityListParaPermissaoTOList(List<Permissao> listPermissaoEntity)
            throws ParseException {

        List<PermissaoTO> listPermissaoTO = new ArrayList<PermissaoTO>();

        if (!ValidacaoUtil.isEmpty(listPermissaoEntity)) {
            for (final Permissao permissaoEntity : listPermissaoEntity) {
                PermissaoTO permissaoTO = new PermissaoTO();

                permissaoTO.setId(permissaoEntity.getId());
                permissaoTO.setName(permissaoEntity.getName());

                listPermissaoTO.add(permissaoTO);
            }
        }
        return listPermissaoTO;
    }

    /**
     * Metodo que converte um PermissaoTO em uma entidade de Permissao Caso o TO
     * venha nulo ou vaziu retorna uma nova entidade Permissao
     * 
     * @param PermissaoTO
     *            O TO de Permissao
     * @return Entity de Parametro Contabilidade
     * @throws ParseException
     */
    public static Permissao PermissaoTOParapermissaoEntity(PermissaoTO permissaoTO) throws ParseException {

        Permissao permissao = new Permissao();
        if (!ValidacaoUtil.isEmpty(permissaoTO)) {
            permissao.setId(permissaoTO.getId());
            permissao.setName(permissaoTO.getName());

        }

        return permissao;
    }

    /**
     * Metodo que converte uma lista de TO de PermissaoTO em uma Lista de
     * entidades de Permissao Caso a lista esteja vazia ou nula retorna uma nova
     * lista de Entidades Permissao
     * 
     * @param listPermissaoTO
     *            A lista de TO's de PermissaoTO
     * @return A lista de Entidades Permissao
     * @throws ParseException
     */
    public static List<Permissao> PermissaoTOListParapermissaoEntityList(List<PermissaoTO> listPermissaoTO) throws ParseException {

        List<Permissao> listPermissao = new ArrayList<Permissao>();

        if (!ValidacaoUtil.isEmpty(listPermissaoTO)) {
            for (final PermissaoTO permissaoTO : listPermissaoTO) {
                Permissao permissao = new Permissao();
                permissao.setId(permissaoTO.getId());
                permissao.setName(permissaoTO.getName());

                listPermissao.add(permissao);
            }
        }

        return listPermissao;
    }
}