/**
 * 
 */
package br.com.boladeneve.festajulina.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import br.com.boladeneve.festajulina.dao.PermissaoDAO;
import br.com.boladeneve.festajulina.model.entity.Permissao;
import br.com.boladeneve.framework.persistence.AbstractDpiJpaDAO;

@Component
@Qualifier("jpa")
public class PermissaoDAOImpl extends AbstractDpiJpaDAO<Permissao> implements PermissaoDAO {

    @Override
    @SuppressWarnings("unchecked")
    public List<Permissao> buscarTodos() {
        final Query query = getEntityManager().createNamedQuery("Permissao.todos", Permissao.class);
        return query.getResultList();
    }
}
