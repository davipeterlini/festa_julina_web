package br.com.boladeneve.festajulina.dao;

import java.util.List;

import br.com.boladeneve.festajulina.model.entity.Usuario;
import br.com.boladeneve.framework.persistence.AbstractDAO;

public interface UsuarioDAO extends AbstractDAO<Usuario> {
    List<Usuario> buscarTodos();
}