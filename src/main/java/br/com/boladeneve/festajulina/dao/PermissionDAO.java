package br.com.boladeneve.festajulina.dao;

import br.com.boladeneve.festajulina.model.entity.Permissao;
import br.com.boladeneve.framework.persistence.AbstractDAO;

/**
 * Data access object for the Permission entity.
 */
public interface PermissionDAO extends AbstractDAO<Permissao> {

	/**
	 * Get the name of Permission by User
	 * 
	 * @param username
	 * @return
	 */
    String findByUsername(String username);		
}