package br.com.boladeneve.festajulina.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import br.com.boladeneve.festajulina.dao.CadFestaJulinaDAO;
import br.com.boladeneve.festajulina.model.entity.CadFestaJulinaEntity;
import br.com.boladeneve.festajulina.model.to.CadFestaJulinaTO;
import br.com.boladeneve.festajulina.table.TableMap;
import br.com.boladeneve.festajulina.util.ValidacaoUtil;
import br.com.boladeneve.framework.persistence.AbstractDpiJpaDAO;

@Component
@Qualifier("jpa")
public class CadFestaJulinaDAOImpl extends
		AbstractDpiJpaDAO<CadFestaJulinaEntity> implements CadFestaJulinaDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> buscarListaCadFestaJulina() throws SQLException {

		final StringBuilder sql = new StringBuilder();

		sql.append("SELECT * FROM CadFestaJulina");
		
		final Query query = getEntityManager().createNativeQuery(sql.toString());
		
		List<Object[]> result = new ArrayList<Object[]>();
		try {
			result = query.getResultList();
		} catch (Exception e) {
			throw new SQLException("message_consulta_sql_error", e);
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> buscaCadFestaJulinaPorCodigoDeCadastro(Integer codigoCadastro) throws SQLException {		
		final StringBuilder sql = new StringBuilder();

		sql.append("SELECT * FROM CadFestaJulina WHERE codigoCadastro = :codigoCadastro");
		
		final Query query = getEntityManager().createNativeQuery(sql.toString());
		query.setParameter("codigoCadastro", codigoCadastro);
		
		List<Object[]> result = new ArrayList<Object[]>();
		try {
			result = query.getResultList();
		} catch (Exception e) {
			throw new SQLException("message_consulta_sql_error", e);
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> buscaCadFestaJulinaPorCampo(CadFestaJulinaTO cadFestaJulinaTO) throws SQLException {
		final StringBuilder sql = new StringBuilder();

		sql.append("SELECT * FROM CadFestaJulina WHERE nome = :nome OR celular = :celular OR email = :email OR facebook = :facebook");
		
		final Query query = getEntityManager().createNativeQuery(sql.toString());
		
		/*Anula o campo caso o mesmo esteja vazio*/
		
		if (ValidacaoUtil.isEmpty(cadFestaJulinaTO.getNome())) {
			cadFestaJulinaTO.setNome("");
		}
		
		if (ValidacaoUtil.isEmpty(cadFestaJulinaTO.getCelular())) {
			cadFestaJulinaTO.setCelular("");
		}
		
		if (ValidacaoUtil.isEmpty(cadFestaJulinaTO.getEmail())) {
			cadFestaJulinaTO.setEmail("");
		}
		
		if (ValidacaoUtil.isEmpty(cadFestaJulinaTO.getFacebook())) {
			cadFestaJulinaTO.setFacebook("");
		}
		
		query.setParameter("nome", cadFestaJulinaTO.getNome());
		query.setParameter("celular", cadFestaJulinaTO.getCelular());
		query.setParameter("email", cadFestaJulinaTO.getEmail());
		query.setParameter("facebook", cadFestaJulinaTO.getFacebook());
		
		List<Object[]> result = new ArrayList<Object[]>();
		try {
			result = query.getResultList();
		} catch (Exception e) {
			throw new SQLException("message_consulta_sql_error", e);
		}
		return result;
	}
	
	@Override
	@Transactional
	public Integer insereCadFestaJulina(CadFestaJulinaTO cadFestaJulinaTO) throws SQLException {
		
		int executeUpdate = 0;

		final StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO " + TableMap.CADFESTAJULINA);
		sql.append(" (codigoCadastro, nome, idade, celular, email, facebook, frequentaigreja, nomeigreja) ");
		sql.append("VALUES (:codigoCadastro, :nome, :idade, :celular, :email, :facebook, :frequentaigreja, :nomeigreja);");

		final Query query = getEntityManager().createNativeQuery(sql.toString());

		if (!ValidacaoUtil.isEmpty(cadFestaJulinaTO.getCodigoCadastro())) {
			query.setParameter("codigoCadastro", cadFestaJulinaTO.getCodigoCadastro());
		}
		
		if (!ValidacaoUtil.isEmpty(cadFestaJulinaTO.getNome())) {
			query.setParameter("nome", cadFestaJulinaTO.getNome());
		}
		
		if (!ValidacaoUtil.isEmpty(cadFestaJulinaTO.getIdade())) {
			query.setParameter("idade", cadFestaJulinaTO.getIdade());
		}

		if (!ValidacaoUtil.isEmpty(cadFestaJulinaTO.getCelular())) {
			query.setParameter("celular", cadFestaJulinaTO.getCelular());
		}

		if (!ValidacaoUtil.isEmpty(cadFestaJulinaTO.getEmail())) {
			query.setParameter("email", cadFestaJulinaTO.getEmail());
		}
		
		if (!ValidacaoUtil.isEmpty(cadFestaJulinaTO.getFacebook())) {
			query.setParameter("facebook", cadFestaJulinaTO.getFacebook());
		}

		if (!ValidacaoUtil.isEmpty(cadFestaJulinaTO.getFrequentaIgreja().toString())) {
			query.setParameter("frequentaigreja", cadFestaJulinaTO.getFrequentaIgreja().toString());
		}
		
		if (!ValidacaoUtil.isEmpty(cadFestaJulinaTO.getNomeIgreja())) {
			query.setParameter("nomeigreja", cadFestaJulinaTO.getNomeIgreja());
		}

		try {
			executeUpdate = query.executeUpdate();
		} catch (Exception e) {
			throw new SQLException("message_insersao_sql_error", e);
		}
		return executeUpdate;
	}
	
	
	/*@Override
	public List<Object[]> buscaUltimoIDCadFestaJulina() throws SQLException {
		final StringBuilder sql = new StringBuilder();

		sql.append("SELECT * FROM CadFestaJulina ORDER BY id DESC LIMIT 1");
		
		final Query query = getEntityManager().createNativeQuery(sql.toString());
		
		List<Object[]> result = new ArrayList<Object[]>();
		try {
			query.getResultList();
		} catch (Exception e) {
			throw new SQLException("message_consulta_sql_error", e);
		}
		return result;
	}
	
	@Override
	public List<Object[]> buscaCadFestaJulinaPorNome(String nome) throws SQLException {
		final StringBuilder sql = new StringBuilder();

		sql.append("SELECT * FROM CadFestaJulina WHERE nome = :nome");
		
		final Query query = getEntityManager().createNativeQuery(sql.toString());
		query.setParameter("nome", nome);
		
		List<Object[]> result = new ArrayList<Object[]>();
		try {
			query.getResultList();
		} catch (Exception e) {
			throw new SQLException("message_consulta_sql_error", e);
		}
		return result;
	}
	
	@Override
	public List<Object[]> buscaCadFestaJulinaPorEmail(String email) throws SQLException {
		final StringBuilder sql = new StringBuilder();

		sql.append("SELECT * FROM CadFestaJulina WHERE email = :email");
		
		final Query query = getEntityManager().createNativeQuery(sql.toString());
		query.setParameter("email", email);
		
		List<Object[]> result = new ArrayList<Object[]>();
		try {
			query.getResultList();
		} catch (Exception e) {
			throw new SQLException("message_consulta_sql_error", e);
		}
		return result;
	}
	
	@Override
	public List<Object[]> buscaCadFestaJulinaPorCelular(String celular) throws SQLException {
		final StringBuilder sql = new StringBuilder();

		sql.append("SELECT * FROM CadFestaJulina WHERE celular = :celular");
		
		final Query query = getEntityManager().createNativeQuery(sql.toString());
		query.setParameter("celular", celular);
		
		List<Object[]> result = new ArrayList<Object[]>();
		try {
			query.getResultList();
		} catch (Exception e) {
			throw new SQLException("message_consulta_sql_error", e);
		}
		return result;
	}
	
	@Override
	public List<Object[]> buscaCadFestaJulinaPorCelular(String facebook) throws SQLException {
		final StringBuilder sql = new StringBuilder();

		sql.append("SELECT * FROM CadFestaJulina WHERE facebook = :facebook");
		
		final Query query = getEntityManager().createNativeQuery(sql.toString());
		query.setParameter("facebook", facebook);
		
		List<Object[]> result = new ArrayList<Object[]>();
		try {
			query.getResultList();
		} catch (Exception e) {
			throw new SQLException("message_consulta_sql_error", e);
		}
		return result;
	}*/

}
