package br.com.boladeneve.festajulina.dao;

import java.util.List;

import br.com.boladeneve.festajulina.model.entity.Permissao;
import br.com.boladeneve.framework.persistence.AbstractDAO;

public interface PermissaoDAO extends AbstractDAO<Permissao> {
    List<Permissao> buscarTodos();
}