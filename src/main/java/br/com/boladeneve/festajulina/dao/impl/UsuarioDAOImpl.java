/**
 * 
 */
package br.com.boladeneve.festajulina.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import br.com.boladeneve.festajulina.dao.UsuarioDAO;
import br.com.boladeneve.festajulina.model.entity.Usuario;
import br.com.boladeneve.framework.persistence.AbstractDpiJpaDAO;

@Component
@Qualifier("jpa")
public class UsuarioDAOImpl extends AbstractDpiJpaDAO<Usuario> implements UsuarioDAO {

    @Override
    @SuppressWarnings("unchecked")
    public List<Usuario> buscarTodos() {
        final Query query = getEntityManager().createNamedQuery("Usuario.Todos", Usuario.class);
        return query.getResultList();
    }
}
