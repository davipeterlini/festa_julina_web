package br.com.boladeneve.festajulina.dao;

import java.sql.SQLException;
import java.util.List;

import br.com.boladeneve.festajulina.excepetion.ValidacaoException;
import br.com.boladeneve.festajulina.model.entity.CadFestaJulinaEntity;
import br.com.boladeneve.festajulina.model.to.CadFestaJulinaTO;
import br.com.boladeneve.framework.persistence.AbstractDAO;

public interface CadFestaJulinaDAO extends AbstractDAO<CadFestaJulinaEntity> {
	
	/**
	 * Busca dados de Cadastro de Pessoa da Festa Julina para a Tela da Tabela
	 * de Cadastro
	 * 
	 * @return Valores encontrados na busca no banco de dados
	 * @throws SQLException 
	 */
	List<Object[]> buscarListaCadFestaJulina() throws SQLException;

	/**
	 * Busca Cadastro de Pessoa da Festa Julina para a Confirmação de Cadastro
	 * (Pulseira) Verifica se pessoa ja foi cadastrada na tabela: CadFestaJulina
	 * 
	 * @return Valor encontrados na busca no banco de dados (CadFestaJulinaTO)
	 * @throws SQLException 
	 */
	List<Object[]> buscaCadFestaJulinaPorCodigoDeCadastro(Integer codigoCadastro) throws SQLException;
	
	/**
	 * Busca Cadastro de Pessoa da Festa Julina para a recuperar cadastro
	 * Verifica se o nome, ou celular, ou email, ou facebook 
	 * ja foi cadastrada na tabela: CadFestaJulina
	 * 
	 * @return Valor encontrados na busca no banco de dados
	 * @throws SQLException 
	 */
	List<Object[]> buscaCadFestaJulinaPorCampo(CadFestaJulinaTO cadFestaJulinaTO)
			throws SQLException;
	
	/**
	 * Insere Cadastro de Pessoa na tabela: CadFestaJulina
	 * 
	 * @param cadFestaJulinaTO
	 * @throws SQLException, ValidacaoException
	 * 
	 */
	Integer insereCadFestaJulina(CadFestaJulinaTO cadFestaJulinaTO)
			throws SQLException, ValidacaoException;

	/**
	 * Verifica o ultimo ID que foi cadastrado na tabela
	 * 
	 * @return Valor encontrado na busca no banco de dados
	 * @throws SQLException 
	 */	
	/*List<Object[]> buscaUltimoIDCadFestaJulina() throws SQLException;*/
	
	/**
	 * Busca Cadastro de Pessoa da Festa Julina para a recuperar cadastro
	 * Verifica se o nome ja foi cadastrada na tabela: CadFestaJulina
	 * 
	 * @return Valor encontrados na busca no banco de dados
	 * @throws SQLException 
	 */
	/*List<Object[]> buscaCadFestaJulinaPorNome(String nome) throws SQLException;*/
	
	/**
	 * Busca Cadastro de Pessoa da Festa Julina para a recuperar cadastro
	 * Verifica se o email ja foi cadastrada na tabela: CadFestaJulina
	 * 
	 * @return Valor encontrados na busca no banco de dados
	 * @throws SQLException 
	 */
	/*List<Object[]> buscaCadFestaJulinaPorEmail(String email) throws SQLException;*/
	
	/**
	 * Busca Cadastro de Pessoa da Festa Julina para a recuperar cadastro
	 * Verifica se o celular ja foi cadastrada na tabela: CadFestaJulina
	 * 
	 * @return Valor encontrados na busca no banco de dados
	 * @throws SQLException 
	 */
	/*List<Object[]> buscaCadFestaJulinaPorCelular(String celular) throws SQLException;*/
	
	/**
	 * Busca Cadastro de Pessoa da Festa Julina para a recuperar cadastro
	 * Verifica se o celular ja foi cadastrada na tabela: CadFestaJulina
	 * 
	 * @return Valor encontrados na busca no banco de dados
	 * @throws SQLException 
	 */
	/*List<Object[]> buscaCadFestaJulinaPorCelular(String celular) throws SQLException;*/
}
