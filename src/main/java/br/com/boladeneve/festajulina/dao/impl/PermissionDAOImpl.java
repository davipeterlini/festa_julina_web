/**
 * 
 */
package br.com.boladeneve.festajulina.dao.impl;

import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import br.com.boladeneve.festajulina.dao.PermissionDAO;
import br.com.boladeneve.festajulina.model.entity.Permissao;
import br.com.boladeneve.framework.persistence.AbstractDpiJpaDAO;

/**
 * Data access object for the Permission entity.
 */
@Component
@Qualifier("jpa")
public class PermissionDAOImpl extends AbstractDpiJpaDAO<Permissao> implements PermissionDAO {
    /**
     * (non-Javadoc)
     * 
     * @see br.com.boladeneve.festajulina.dao.PermissionDAO#findByUserName(br.com.boladeneve.festajulina.model.entity.Permissao)
     */
    @Override
    public String findByUsername(String username) {

        final StringBuffer sbQuery = new StringBuffer();
        sbQuery.append("select p.NAME ");
        sbQuery.append("from SEC_USER_PERMISSION up ");
        sbQuery.append("inner join SEC_PERMISSION p on p.ID = up.ID_PERMISSION ");
        sbQuery.append("inner join SEC_USER u on u.ID = up.ID_USER ");
        sbQuery.append("where u.USER_NAME = ?");

        Query query = getEntityManager().createNativeQuery(sbQuery.toString());
        query.setParameter(1, username);

        return (String) query.getSingleResult();
    }
}
