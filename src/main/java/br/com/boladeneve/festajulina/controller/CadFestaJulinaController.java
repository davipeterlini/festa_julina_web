package br.com.boladeneve.festajulina.controller;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import br.com.boladeneve.festajulina.excepetion.ValidacaoException;
import br.com.boladeneve.festajulina.model.to.CadFestaJulinaTO;
import br.com.boladeneve.festajulina.service.CadFestaJulinaService;
import br.com.boladeneve.framework.web.MessageFactory;

@Component(value = "cadFestaJulinaController")
@Scope("view") 
public class CadFestaJulinaController {

	@Autowired
	private CadFestaJulinaService cadFestaJulinaService;

	private CadFestaJulinaTO cadFestaJulinaTO;

	private List<CadFestaJulinaTO> listCadFestaJulinaTO;

	private FacesMessage facesMessage;

	@PostConstruct
	public void initialize() throws ValidacaoException, ParseException, SQLException {
		this.setCadFestaJulinaTO(new CadFestaJulinaTO());
		this.setListCadFestaJulinaTO(new ArrayList<CadFestaJulinaTO>());

		buscarListaCadFestaJulina();
	}
	
	private void buscarListaCadFestaJulina() throws ParseException, SQLException {
		listCadFestaJulinaTO = cadFestaJulinaService.buscarListaCadFestaJulina();
	}

	public void buscaCadFestaJulinaPorCodigoDeCadastro() throws ParseException, SQLException {
		listCadFestaJulinaTO = cadFestaJulinaService.buscaCadFestaJulinaPorCodigoDeCadastro(cadFestaJulinaTO.getCodigoCadastro());
	
		imprimeMessagem(listCadFestaJulinaTO);	
	}
		
	public void buscaCadFestaJulinaPorCampo() throws ParseException, SQLException {		
		listCadFestaJulinaTO = cadFestaJulinaService.buscaCadFestaJulinaPorCampo(cadFestaJulinaTO);
		
		imprimeMessagem(listCadFestaJulinaTO);	
	}
	
	public String insereCadFestaJulina() throws Exception {
		listCadFestaJulinaTO = new ArrayList<CadFestaJulinaTO>();
		listCadFestaJulinaTO.add(getCadFestaJulinaTO());
		
		Integer insereDadoBanco = cadFestaJulinaService.insereCadFestaJulina(listCadFestaJulinaTO);
		
		if (!insereDadoBanco.equals(0)) {
			imprimeMessagem(listCadFestaJulinaTO);
		} else {
			imprimeMessagem(new ArrayList<CadFestaJulinaTO>());
		}
		return "/pages/confirmacaoCadastro?faces-redirect=true";
	}
	
	private void imprimeMessagem(List<CadFestaJulinaTO> listCadFestaJulinaTO) {
		List<String> contemItens = new ArrayList<String>();
		
		if (!listCadFestaJulinaTO.isEmpty()) {
			contemItens.add(MessageFactory.getMessage("message_cad_ja_efetuado"));
			
			for (CadFestaJulinaTO cadFestaJulinaTO : listCadFestaJulinaTO) {
				contemItens.add(" Código de Cadastro: " + cadFestaJulinaTO.getCodigoCadastro() + ", Nome: " + cadFestaJulinaTO.getNome());
				if (cadFestaJulinaTO.getFrequentaIgreja().equals("S")) {
					contemItens.add(MessageFactory.getMessage("message_pulseira_crente"));
				}else {
					contemItens.add(MessageFactory.getMessage("message_pulseira_nao_crente"));
				}
			}
		}else {
			contemItens.add(MessageFactory.getMessage("message_usuario_nao_cadastrado"));
		}
		
		imprimeMensagemRequestContext(contemItens);
	}
	
	private void imprimeMensagemRequestContext(List<String> contemItens) {

		String replace = contemItens.toString().replaceAll("\\[", "").replaceAll("\\]", "");
		
		if (contemItens.contains(MessageFactory.getMessage("message_usuario_nao_cadastrado"))) {
			facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, replace, "");
		} else {
			facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, replace.replaceAll(",", ";  "), "");
		}			
		
		FacesContext.getCurrentInstance().addMessage("", facesMessage);
		RequestContext.getCurrentInstance().showMessageInDialog(facesMessage);
	}

	public CadFestaJulinaTO getCadFestaJulinaTO() {
		return cadFestaJulinaTO;
	}

	public void setCadFestaJulinaTO(CadFestaJulinaTO cadFestaJulinaTO) {
		this.cadFestaJulinaTO = cadFestaJulinaTO;
	}

	public List<CadFestaJulinaTO> getListCadFestaJulinaTO() {
		return listCadFestaJulinaTO;
	}

	public void setListCadFestaJulinaTO(
			List<CadFestaJulinaTO> listCadFestaJulinaTO) {
		this.listCadFestaJulinaTO = listCadFestaJulinaTO;
	}
	
	
	/*	public void buscaCadFestaJulinaPorNome() throws ParseException {
		listCadFestaJulinaTO = cadFestaJulinaService.buscaCadFestaJulinaPorNome(cadFestaJulinaTO.getNome());
				
		imprimeMessagem(listCadFestaJulinaTO);	
	}
	
	public void buscaCadFestaJulinaPorEmail() throws ParseException {
		listCadFestaJulinaTO = cadFestaJulinaService.buscaCadFestaJulinaPorEmail(cadFestaJulinaTO.getEmail());
				
		imprimeMessagem(listCadFestaJulinaTO);	
	}
	
	public void buscaCadFestaJulinaPorCelular() throws ParseException {
		listCadFestaJulinaTO = cadFestaJulinaService.buscaCadFestaJulinaPorCelular(cadFestaJulinaTO.getCelular());
				
		imprimeMessagem(listCadFestaJulinaTO);	
	}
	
	public void buscaCadFestaJulinaPorFacebook() throws ParseException {
		listCadFestaJulinaTO = cadFestaJulinaService.buscaCadFestaJulinaPorFacebook(cadFestaJulinaTO.getFacebook());
				
		imprimeMessagem(listCadFestaJulinaTO);	
	}*/

}
