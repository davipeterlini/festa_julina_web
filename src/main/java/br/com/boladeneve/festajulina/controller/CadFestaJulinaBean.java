package br.com.boladeneve.festajulina.controller;
 
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
 
@ManagedBean
public class CadFestaJulinaBean {
     
    private void testBean(){
    	FacesContext context = FacesContext.getCurrentInstance();
	    Map<String,String> params = context.getExternalContext().getRequestParameterMap();
	    String email = params.get("email");
	    System.out.println("##############################");
	    System.out.println(email);
	    System.out.println("##############################");
    }
 
}