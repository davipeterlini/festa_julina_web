package br.com.boladeneve.festajulina.controller;

import java.text.ParseException;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import br.com.boladeneve.festajulina.model.to.LoginTO;
import br.com.boladeneve.festajulina.service.PermissionService;
import br.com.boladeneve.framework.util.JSFUtil;
import br.com.boladeneve.framework.web.MessageFactory;

/**
 * The Class LoginController.
 */
@Component(value = "loginController")
@Scope("request")
public class LoginController {

    private String username;
    private String password;

    @Autowired(required = true)
    @Qualifier("authenticationManager")
    private AuthenticationManager authenticationManager;

    @Autowired
    private PermissionService permissionService;

    @PostConstruct
    public void init() {
        setUsername(StringUtils.EMPTY);
        setPassword(StringUtils.EMPTY);
    }

    /**
     * Login.
     * 
     * @return the string
     * @throws ParseException
     */
    public String login() throws ParseException {
        try {
            Authentication request = new UsernamePasswordAuthenticationToken(
                    this.getUsername(), this.getPassword());
            Authentication result = authenticationManager.authenticate(request);
            SecurityContextHolder.getContext().setAuthentication(result);

            JSFUtil.setSessionAtribute("user", username);

            final String permission = permissionService
                    .findPermissionByUsername(username);

            JSFUtil.setSessionAtribute("permission", permission);

            LoginTO loginTO = new LoginTO();
            loginTO.setUsuario(username);
            loginTO.setPermissao(permission);
            JSFUtil.setSessionAtribute("loginTO", loginTO);

        } catch (AuthenticationException e) {
            FacesMessage facesMessage = MessageFactory.getMessage(
                    "message_invalid_username_or_password",
                    FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            return null;
        }
        return "/pages/buscaCadFestaJulina?faces-redirect=true";
    }
    
    /**
     * Mobile.
     * 
     * @return the string
     * @throws ParseException
     */
    public String loginMobile() throws ParseException {
        try {
            Authentication request = new UsernamePasswordAuthenticationToken(
                    this.getUsername(), this.getPassword());
            Authentication result = authenticationManager.authenticate(request);
            SecurityContextHolder.getContext().setAuthentication(result);

            JSFUtil.setSessionAtribute("user", username);

            final String permission = permissionService
                    .findPermissionByUsername(username);

            JSFUtil.setSessionAtribute("permission", permission);

            LoginTO loginTO = new LoginTO();
            loginTO.setUsuario(username);
            loginTO.setPermissao(permission);
            JSFUtil.setSessionAtribute("loginTO", loginTO);

        } catch (AuthenticationException e) {
            FacesMessage facesMessage = MessageFactory.getMessage(
                    "message_invalid_username_or_password",
                    FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            return null;
        }
        return "/pages/buscaCadFestaJulinaMobile?faces-redirect=true";
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}