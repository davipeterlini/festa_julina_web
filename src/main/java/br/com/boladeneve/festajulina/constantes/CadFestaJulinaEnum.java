package br.com.boladeneve.festajulina.constantes;

public enum CadFestaJulinaEnum {

	SIM("S"),	
	NAO("N");
		
	private final String menemonico;

	public String getMenemonico() {
		return menemonico;
	}
	
	private CadFestaJulinaEnum(String menemonico) {
		this.menemonico = menemonico;
	}
	
	@Override
	public String toString() { 
		return menemonico.toString();
	}
	
}