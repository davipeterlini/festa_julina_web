package br.com.boladeneve.festajulina.validation;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.primefaces.validate.ClientValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import br.com.boladeneve.festajulina.model.to.CadFestaJulinaTO;
import br.com.boladeneve.festajulina.service.CadFestaJulinaService;
import br.com.boladeneve.framework.web.MessageFactory;
 
/**
 * Custom JSF Validator for Celular input
 */
/*@FacesValidator("emailValidator")*/
@Component
@Scope("request")
public class CelularValidator implements Validator, ClientValidator {
	
	@Autowired
	private CadFestaJulinaService cadFestaJulinaService;
 
    private Pattern pattern;
  
    //private static final String CELLPHONE_PATTERN = "(\(11\) [9][0-9]{4}-[0-9]{4})|(\(1[2-9]\) [5-9][0-9]{3}-[0-9]{4})|(\([2-9][1-9]\) [5-9][0-9]{3}-[0-9]{4})$";
    
    private static final String CELLPHONE_PATTERN = "\\([0-9][0-9]\\)\\s[9][0-9][0-9][0-9][0-9]\\-[0-9][0-9][0-9][0-9]";
    
    public CelularValidator() {
        pattern = Pattern.compile(CELLPHONE_PATTERN);
    }
 
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
    	List<CadFestaJulinaTO> listCadFestaJulinaTO = new ArrayList<CadFestaJulinaTO>();
    	
    	if(value == null) {
            return;
        }

    	// Consulta no Banco
    	//String replace = value.toString().replaceAll("\\(", "").replaceAll("\\)", "").replaceAll("\\ ", "").replaceAll("\\-", "");
         
        if(!pattern.matcher(value.toString()).matches()) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, " ", MessageFactory.getMessage("message_cad_cel_invalido")));
        }
        
        String swichtParameter = (String) component.getAttributes().get("swichtParameter"); 
        
        if (!value.toString().equals(MessageFactory.getMessage("message_cad_facebook_padrao"))) {
        	if (swichtParameter.contains("cad")) {
                
        		try { 
        			CadFestaJulinaTO cadFestaJulinaTO = new CadFestaJulinaTO(); 
        	        cadFestaJulinaTO.setCelular(value.toString()); 
        	        
        			listCadFestaJulinaTO = cadFestaJulinaService.buscaCadFestaJulinaPorCampo(cadFestaJulinaTO);
        			
        	        if (!listCadFestaJulinaTO.isEmpty()) {
        	        	throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_WARN, " ", MessageFactory.getMessage("message_cad_celular_ja_cadastrado")));
        			} else { 
        				throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_INFO, "", " OK!"));
        			}
        		} catch (ParseException e) {
        			erro_consulta();
        		} catch (SQLException e) {
        			erro_consulta();
    			}
    		}
        }
        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_INFO, "", " OK!"));
        
    }

	private void erro_consulta() {
		throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Database Error ", MessageFactory.getMessage("message_consulta_sql_error")));
	}
 
    public Map<String, Object> getMetadata() {
        return null;
    }
 
    public String getValidatorId() {
        return "emailValidator";
    }
     
}