package br.com.boladeneve.festajulina.validation;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.primefaces.validate.ClientValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import br.com.boladeneve.festajulina.model.to.CadFestaJulinaTO;
import br.com.boladeneve.festajulina.service.CadFestaJulinaService;
import br.com.boladeneve.framework.web.MessageFactory;
 
/**
 * Custom JSF Validator for Email input
 */
/*@FacesValidator("emailValidator")*/
@Component
@Scope("request")
public class EmailValidator implements Validator, ClientValidator {
	
	@Autowired
	private CadFestaJulinaService cadFestaJulinaService;
 
    private Pattern pattern;
  
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                                                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
  
    public EmailValidator() {
        pattern = Pattern.compile(EMAIL_PATTERN);
    }
 
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
    	List<CadFestaJulinaTO> listCadFestaJulinaTO = new ArrayList<CadFestaJulinaTO>();
    	
    	if(value == null) {
            return;
        }
         
        if(!pattern.matcher(value.toString()).matches()) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, " ", MessageFactory.getMessage("message_cad_email_invalido")));
        }
        
        String swichtParameter = (String) component.getAttributes().get("swichtParameter"); 
        
        if (!value.toString().equals(MessageFactory.getMessage("message_cad_email_padrao"))) {
        	if (swichtParameter.contains("cad")) {
                
        		try {
        			CadFestaJulinaTO cadFestaJulinaTO = new CadFestaJulinaTO(); 
        	        cadFestaJulinaTO.setEmail(value.toString()); 
        	        
        			listCadFestaJulinaTO = cadFestaJulinaService.buscaCadFestaJulinaPorCampo(cadFestaJulinaTO);
        	        if (!listCadFestaJulinaTO.isEmpty()) {
        	        	throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_WARN, " ", MessageFactory.getMessage("message_cad_email_ja_cadastrado")));
        			} else { 
        				throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_INFO, "", " OK!"));
        			}
        		} catch (ParseException e) {
        			erro_consulta();
        		} catch (SQLException e) {
        			erro_consulta();
    			}
    		}
		}
        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_INFO, "", " OK!"));
        
    }

	private void erro_consulta() {
		throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Database Error ", MessageFactory.getMessage("message_consulta_sql_error")));
	}
 
    public Map<String, Object> getMetadata() {
        return null;
    }
 
    public String getValidatorId() {
        return "emailValidator";
    }
     
}