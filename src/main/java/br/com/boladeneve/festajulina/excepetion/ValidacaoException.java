package br.com.boladeneve.festajulina.excepetion;

public class ValidacaoException extends Exception {
	
	private static final long serialVersionUID = 1L;

	/**
	 * Instancia uma nova exceção de validação.
	 */
	public ValidacaoException(final String message) {
		super(message);
	}
}