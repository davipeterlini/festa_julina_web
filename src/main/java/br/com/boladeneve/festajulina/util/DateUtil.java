package br.com.boladeneve.festajulina.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

    /**
     * Format some {@link Date} into {@link Timestamp}
     * 
     * @param date
     *            {@link Date} to be formated.
     * @return Formated {@link Date} into {@link Timestamp}
     * @throws ParseException
     *             If there was an error in conversion.
     */
    public static Timestamp dateToTimestamp(final Date date)
            throws ParseException {

        Timestamp convertedDate = null;

        if (!ValidacaoUtil.isEmpty(date)) {
            final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            final String dateString = sdf.format(date);
            final Date formatedDate = sdf.parse(dateString);
            convertedDate = new Timestamp(formatedDate.getTime());
        }

        return convertedDate;
    }

    public static Timestamp utilDateToSQLTimestamp(final Date date)
            throws ParseException {
        return new Timestamp(date.getTime());
    }

    public static java.util.Date timestampToUtilDate(final Timestamp ts)
            throws ParseException {
        return new java.util.Date(ts.getTime());
    }

    public static Timestamp stringToTimestamp(final String data)
            throws ParseException {
        Timestamp timestamp = null;
        final SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");

        final Calendar c = Calendar.getInstance();
        c.setTime(format.parse(data));
        timestamp = new Timestamp(c.getTimeInMillis());

        return timestamp;
    }

    public static String timestampToString(final Timestamp ts) {
        return new SimpleDateFormat("yyyy-MM-ddTHH:mm:ss").format(ts);
    }

    public static String timestampToStringBr(final Timestamp ts) {
        return new SimpleDateFormat("dd/MM/yyyy").format(ts);
    }

    public static String javaUtilDateToString(final java.util.Date date) {
        return new SimpleDateFormat("yyyy-MM-ddTHH:mm:ss").format(date);
    }

    public static String javaUtilDateToStringApenasDataPTBR(
            final java.util.Date date) {
        return new SimpleDateFormat("dd/MM/yyyy").format(date);
    }
    
    public static String javaUtilDateToStringApenasDataPTBRUnderLine(
            final java.util.Date date) {
        return new SimpleDateFormat("dd_MM_yyyy").format(date);
    }

    public static String javaUtilDateToStringDB2(final java.util.Date date) {
        return new SimpleDateFormat("yyyyMMdd").format(date);
    }

    public static java.util.Date stringToJavaUtilDate(final String data)
            throws ParseException {
        java.util.Date date = null;
        final SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");

        final Calendar c = Calendar.getInstance();
        c.setTime(format.parse(data));
        date = new Date(c.getTimeInMillis());

        return date;
    }

    public static String javaUtilDateToStringAnoMes(final java.util.Date date) {
        return new SimpleDateFormat("yyyyMM").format(date);
    }

    public static Timestamp dataAtualTimestamp() {
        Calendar c = Calendar.getInstance();
        return new Timestamp(c.getTimeInMillis());
    }
    
    public static Date primeiroDiaMes(final Date data){
        
        Calendar cal = Calendar.getInstance();
        Calendar primeiroDia = Calendar.getInstance();
        
        cal.setTime(data);
        primeiroDia.setTime(data);
        
        //1º dia do mês atual
        primeiroDia.add(Calendar.DAY_OF_MONTH, - cal.get(Calendar.DAY_OF_MONTH));
        primeiroDia.add(Calendar.DAY_OF_YEAR, 1);
        
                
        return new Date(primeiroDia.getTimeInMillis());         
    }
    
    public static  Date ultimoDiaMes(final Date data){
        
        Calendar cal = Calendar.getInstance();
        Calendar ultimoDia = Calendar.getInstance();
        
        //Ultimo dia do mês atual
        ultimoDia.add(Calendar.MONTH, 1);
        ultimoDia.add(Calendar.DAY_OF_MONTH, -cal.get(Calendar.DAY_OF_MONTH));
        
        return new Date(ultimoDia.getTimeInMillis());        
    }
    
    
}