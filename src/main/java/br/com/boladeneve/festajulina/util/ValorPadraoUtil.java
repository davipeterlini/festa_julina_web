package br.com.boladeneve.festajulina.util;

import org.apache.commons.lang3.StringUtils;

/**
 * Classe para métodos que retornam um valor padrão quando é nulo
 * 
 * @author wsantana
 *
 */
public abstract class ValorPadraoUtil {

    /**
     * Verifica se o valor é nulo
     * 
     * @param valor Valor a ser validado
     * @return {@code valor} é nulo retorna StringUtils.EMPTY <br>
     *         {@code valor} não é nulo retorna o valor
     */
    public static String retornaValorPadrao(final String valor) {
        return retornaValorPadrao(valor, StringUtils.EMPTY);
    }
    
    /**
     * Verifica se o valor é nulo informando um valor padrão
     * 
     * @param valor Valor a ser validado
     * @param padrao Valor padrão
     * @return 
     */
    public static String retornaValorPadrao(final String valor, final String padrao) {
        return (ValidacaoUtil.isEmpty(valor)) ? padrao : valor;
    }
    
    /**
     * Verifica se o valor do objeto é nulo informando um valor padrão
     * 
     * @param valor Valor a ser validado
     * @param padrao Valor padrão
     * @return
     */
    public static String retornaValorPadrao(final Object valor, final String padrao) {
        return ValidacaoUtil.isEmpty(valor) ? padrao : valor.toString();
    }
}