package br.com.boladeneve.festajulina.util;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import br.com.boladeneve.festajulina.excepetion.ValidacaoException;
import br.com.boladeneve.festajulina.model.to.ObrigatoriedadeCampoTO;
import br.com.boladeneve.framework.web.Messages;

public abstract class ValidacaoUtil {

    private static final Integer ZERO = Integer.valueOf(0);

    public static boolean isEmpty(final Object param) {
        if (param == null) {
            return true;
        } else if (param instanceof String) {
            return ((String) param).isEmpty();
        } else if (param instanceof BigDecimal) {
            return false;
        } else if (param instanceof Integer) {
            return false;
        } else if (param instanceof Double) {
            return false;
        } else if (param instanceof Date) {
            return false;
        } else if (param instanceof Object[]) {
            return ((Object[]) param).length < 1;
        } else if (param instanceof Collection<?>) {
            return ((Collection<?>) param).isEmpty();
        } else if (param instanceof Map<?, ?>) {
            return ((Map<?, ?>) param).isEmpty();
        }
        return false;
    }
    
    /**
     * Verifica se todos os parametros estão vazios.
     * 
     * @param params
     *            Lista de parametros a serem validados.
     * @return <code>true: </code> Todos os parametros estão vazios. <br/>
     *         <code>false: </code> Ao menos um parametro esta preenchido.
     */
    public static boolean isEmpty(final Object... params) {
        for (final Object param : params) {
            if (!isEmpty(param)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Valida se todos os campos mapeados estão preenchidos.
     * 
     * @param campos
     *            Map portador de todos os campos a serem validados. - Chave:
     *            Nome do campo. - Valor: Valor do campo.
     */
    public static void validaCamposObrigatorios(
            final LinkedHashMap<String, String> campos)
                    throws ValidacaoException {

        final Iterator<Entry<String, String>> campoIterator = campos.entrySet()
                .iterator();

        while (campoIterator.hasNext()) {
            final Map.Entry<String, String> campo = (Entry<String, String>) campoIterator
                    .next();
            if (isEmpty(campo.getValue())) {
                final String mensagem = Messages
                        .getMessage("campo_obrigatorio");
                throw new ValidacaoException(
                        MessageFormat.format(mensagem, campo.getKey()));
            }
        }
    }

    /**
     * Valida se determinada intervalo de data está correto, ou seja, se a
     * primeira data é menor que a segunda.
     * 
     * @param dtInicial
     *            Data inicial do intervalo.
     * @param dtFinal
     *            Data final do intervalo.
     * @param campoDtInicial
     *            Nome do campo referente a data inicial.
     * @param campoDtFinal
     *            Nome do campo referente a data final.
     * @throws ValidacaoException
     *             Intervalo de data inválido.
     */
    public static void validaIntervaloData(final Date dtInicial,
            final Date dtFinal, final String campoDtInicial,
            final String campoDtFinal) throws ValidacaoException {
        if (dtInicial.compareTo(dtFinal) >= 0) {
            String menssagem = Messages.getMessage("intervalo_invalido");
            menssagem = MessageFormat.format(menssagem, campoDtInicial,
                    campoDtFinal);
            throw new ValidacaoException(menssagem);
        }
    }

    /**
     * Valida se determinada a data informada é maior que a data atual
     * 
     * @param dataInformada
     *            Data informada.
     * @param campoDataInformada
     *            Nome do campo referente a data.
     * @throws ValidacaoException
     *             Intervalo de data inválido.
     */
    public static void validaIntervaloDataAtual(final Date dataInformada,
            final String campoDataInformada) throws ValidacaoException {
        final Date dtInicial = new Date();

        if (dtInicial.compareTo(dataInformada) >= 0) {
            String menssagem = Messages
                    .getMessage("intervalo_invalido_data_atual");
            menssagem = MessageFormat.format(menssagem, campoDataInformada);
            throw new ValidacaoException(menssagem);
        }
    }

    /**
     * Valida se todos os campos mapeados estão preenchidos. Retorna mensagem de
     * obrigatoriedade de pelo menos 1 campo preenchido.
     * 
     * @param campos
     *            Lista de {@link ObrigatoriedadeCampoTO}
     */
    public static void validaObrigatoriedadeUmCampo(
            final List<ObrigatoriedadeCampoTO> campos)
                    throws ValidacaoException {

        for (final ObrigatoriedadeCampoTO campo : campos) {

            if (isEmpty(campo.getValorCampo())) {

                final String mensagem;
                if (campo.getArtigoMasc()) {
                    mensagem = Messages
                            .getMessage("campo_obrigatorio_um_campo_m");
                } else {
                    mensagem = Messages
                            .getMessage("campo_obrigatorio_um_campo_f");
                }

                throw new ValidacaoException(
                        MessageFormat.format(mensagem, campo.getNomeCampo()));
            }
        }
    }

    /**
     * Valida se determinada intervalo de data está correto, ou seja, se a
     * primeira data é menor ou igaual que a segunda.
     * 
     * @param dtInicial
     *            Data inicial do intervalo.
     * @param dtFinal
     *            Data final do intervalo.
     * @param campoDtInicial
     *            Nome do campo referente a data inicial.
     * @param campoDtFinal
     *            Nome do campo referente a data final.
     * @throws ValidacaoException
     *             Intervalo de data inválido.
     */
    public static void validaIntervaloDataMenorIgual(final Date dtInicial,
            final Date dtFinal, final String campoDtInicial,
            final String campoDtFinal) throws ValidacaoException {
        if (dtInicial.after(dtFinal)) {
            String menssagem = Messages
                    .getMessage("intervalo_invalido_menor_igaul");
            menssagem = MessageFormat.format(menssagem, campoDtInicial,
                    campoDtFinal);
            throw new ValidacaoException(menssagem);
        }
    }

    /**
     * Valida se determinada a data informada maior que o primeiro dia do mês
     * atual
     * 
     * @param dataInformada
     *            Data informada.
     * @param campoDataInformada
     *            Nome do campo referente a data.
     * @throws ValidacaoException
     *             Intervalo de data inválido.
     */
    public static void validaDataPrimeiroDiaMes(final Date dataInformada,
            final String campoDataInformada) throws ValidacaoException {

        final Calendar dataInicial = Calendar.getInstance();
        dataInicial.set(Calendar.DAY_OF_MONTH, 1);
        dataInicial.set(Calendar.HOUR_OF_DAY, ZERO);
        dataInicial.set(Calendar.MINUTE, ZERO);
        dataInicial.set(Calendar.SECOND, ZERO);
        dataInicial.set(Calendar.MILLISECOND, ZERO);

        if (dataInformada.compareTo(dataInicial.getTime()) < 0) {
            String menssagem = Messages
                    .getMessage("intervalo_invalido_primeiro_dia_mes");
            menssagem = MessageFormat.format(menssagem, campoDataInformada);

            throw new ValidacaoException(menssagem);
        }
    }
}