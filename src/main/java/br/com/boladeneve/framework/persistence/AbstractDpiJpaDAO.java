package br.com.boladeneve.framework.persistence;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

/**
 * Essa classe abstrata tem como objetivo simplificar a camada DAO com Spring e JPA.
 * 
 * Ela irá lidar com transações do banco de dados SQL SERVER.
 * Todos os DAOs que utilizam a conexão com SQL SERVER DPI devem extendê-la.
 *
 * @param <T> Tipo genérico.
 */
public abstract class AbstractDpiJpaDAO<T extends Serializable> implements AbstractDAO<T> {

	/** The clazz. */
	private Class<T> clazz;

	/** The entity manager. */
	@PersistenceContext(unitName = "persistenceUnit")
	private EntityManager entityManager;
	
	@Value("hibernate.jdbc.batch_size")
	private String batchSize;

	/**
	 * Instantiates a new abstract jpa dao.
	 */
	@SuppressWarnings({ "unchecked" })
	public AbstractDpiJpaDAO() {
		Type type = getClass().getGenericSuperclass();
		Type[] arguments = ((ParameterizedType) type).getActualTypeArguments();
		this.clazz = (Class<T>) arguments[0];
	}
		
    public <T extends Class> Collection<T> bulkSave(Collection<T> entities) {
        final List<T> salvarEntities = new ArrayList<T>(entities.size());
        int i = 0;
        for (T t : entities) {
            entityManager.persist(t);
            salvarEntities.add(t);

            i++;
            if (i % Integer.valueOf(batchSize) == 0) {
                entityManager.flush();
                entityManager.clear();
            }
        }
        entityManager.flush();
        entityManager.clear();
        return salvarEntities;
    }

	/**
	 * Gets the entity manager.
	 *
	 * @return the entity manager
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Sets the entity manager.
	 *
	 * @param entityManager
	 *            the new entity manager   
	 */
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/**
	 * Sets the clazz.
	 *
	 * @param clazzToSet
	 *            the new clazz
	 */
	public void setClazz(final Class<T> clazzToSet) {
		this.clazz = clazzToSet;
	}

	/**
	 * @see br.com.boladeneve.framework.persistence.AbstractDAO#buscarPorId(java.lang.Long)
	 */
	public T buscarPorId(final Long id) {
		return entityManager.find(clazz, id);
	}
	
	/**
	 * @see br.com.boladeneve.framework.persistence.AbstractDAO#buscarPorId(java.lang.Integer)
	 */
	public T buscarPorId(final Integer id) {
		return entityManager.find(clazz, id);
	}

	/**
	 * @see br.com.boladeneve.framework.persistence.AbstractDAO#buscarTodos()
	 */
	@SuppressWarnings("unchecked")
	public List<T> buscarTodos() {
		return entityManager.createQuery("from " + clazz.getName()).getResultList();
	}

	/**
	 * @see br.com.boladeneve.framework.persistence.AbstractDAO#salvar(java.io.Serializable)
	 */
	@Transactional
	public void salvar(final T entity) {
		entityManager.persist(entity);
		entityManager.flush();
	}

	/**
	 * @see br.com.boladeneve.framework.persistence.AbstractDAO#atualizar(java.io.Serializable)
	 */
	@Transactional
	public T atualizar(final T entity) {
		return entityManager.merge(entity);
	}

	/**
	 * @see br.com.boladeneve.framework.persistence.AbstractDAO#deletarPorId(java.lang.Long)
	 */
	@Transactional
	public void deletarPorId(final Long entityId) {
		final T entity = buscarPorId(entityId);
		deletar(entity);
		entityManager.flush();
	}

	/**
	 * @see br.com.boladeneve.framework.persistence.AbstractDAO#deletar(java.io.Serializable)
	 */
	@Transactional
	public void deletar(final T entity) {
		if (entity != null) {
			entityManager.remove(entity);
		}
	}
}