package br.com.boladeneve.framework.persistence;

import java.io.Serializable;
import java.util.List;

/**
 * Interface com todos os métodos comuns de persistência.
 * 
 * @param <T>
 */
public interface AbstractDAO<T extends Serializable> {

	/**
	 * Obtém todas as entidades por id. 
	 * 
	 * @param id
	 * @return
	 */
	T buscarPorId(final Long id);
	
	/**
	 * Obtém todas as entidades por id. 
	 * 
	 * @param id
	 * @return
	 */
	T buscarPorId(final Integer id);
	
	/**
	 * Obtém todas as entidades.
	 * 
	 * @return
	 */
	List<T> buscarTodos();
	
	/**
	 * Persiste a entidade.
	 * 
	 * @param entidade
	 */
	void salvar(final T entidade);
	
	/**
	 * Atualiza a entidade.
	 * 
	 * @param entidade
	 */
	T atualizar(final T entidade);
	
	/**
	 * Deleta a entidade por id.  
	 * 
	 * @param 
	 */
	void deletarPorId(final Long idEntidade);
	
	/**
	 * Deleta entidade.
	 * 
	 * @param entidade
	 */
	void deletar(final T entidade);
}