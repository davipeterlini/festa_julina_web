package br.com.boladeneve.framework.util;

import java.io.IOException;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 * Métodos utilitários para camada de apresentação
 * 
 * @author wsantana
 *
 */
public class JSFUtil {

    public static ExternalContext getExternalContext() {
        final FacesContext facesContext = FacesContext.getCurrentInstance();
        return facesContext.getExternalContext();
    }

    public static Object getSessionAtribute(final String parameterName) {
        final Map<String, Object> paramMap = getExternalContext().getSessionMap();
        return paramMap.get(parameterName);
    }

    public static void setSessionAtribute(final String key, final Object object) {
        final Map<String, Object> paramMap = getExternalContext().getSessionMap();
        paramMap.put(key, object);
    }

    public static void forwardPage(final String page) throws IOException {
        getExternalContext().redirect(getExternalContext().getRequestContextPath() + page);
    }

    /**
     * Obtem um parametro guardado na requisicao
     * 
     * @param parameterName
     *            identificacao do parametro
     * @return parametro que foi guardado
     */
    public static String getRequestParameter(final String parameterName) {
        final Map<String, String> paramMap = getExternalContext().getRequestParameterMap();
        return paramMap.get(parameterName);
    }

    /**
     * Remove um atributo do mapa de parametros de requisiÃ§Ã£o.
     * 
     * @param attributeName
     *            identificacao do atributo
     */
    public static void removeRequestParameter(final String attributeName) {
        final Map<String, Object> paramMap = getExternalContext().getRequestMap();
        paramMap.remove(attributeName);
    }

    /**
     * Remove um atributo da sessao
     * 
     * @param attributeName
     *            identificacao do atributo
     */
    public static void removeSessionAttribute(final String attributeName) {
        final Map<String, Object> paramMap = getExternalContext().getSessionMap();
        paramMap.remove(attributeName);
    }
}
